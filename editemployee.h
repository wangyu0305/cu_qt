#ifndef EDITEMPLOYEE_H
#define EDITEMPLOYEE_H
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/

#include <QDialog>
#include <QSqlQuery>

namespace Ui {
class editEmployee;
}

class editEmployee : public QDialog
{
    Q_OBJECT

public:
    explicit editEmployee(QWidget *parent = 0, int id = 0);
    void setId(int);
    void initDisplay();
    void display(QSqlQuery);
    ~editEmployee();

private slots:
    void on_comboBox_5_currentIndexChanged(const QString &arg1);

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::editEmployee *ui;
    int eid;
    int rid;
};

#endif // EDITEMPLOYEE_H
