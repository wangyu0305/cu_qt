#ifndef VIEWPAYROLLSUMMARYNOTICY_H
#define VIEWPAYROLLSUMMARYNOTICY_H
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include <QDialog>
#include <QStandardItemModel>

namespace Ui {
class ViewPayrollSummaryNoticy;
}

class ViewPayrollSummaryNoticy : public QDialog
{
    Q_OBJECT

public:
    explicit ViewPayrollSummaryNoticy(QWidget *parent = 0, int max = 0);
    ~ViewPayrollSummaryNoticy();
    void setTableHeader();
    void select(int max);

private slots:
    void on_pushButton_clicked();

private:
    Ui::ViewPayrollSummaryNoticy *ui;
    QStandardItemModel *model;
};

#endif // VIEWPAYROLLSUMMARYNOTICY_H
