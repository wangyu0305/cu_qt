/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include "employeemanagementcontrol.h"
#include "ui_em.h"
#include <QStandardItemModel>
#include "addrole.h"
#include "newemp.h"
#include "psoption.h"
#include "editemployee.h"
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QMessageBox>
#include "employee.h"

EmployeeManagementControl::EmployeeManagementControl(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EmployeeManagementControl)
{
    ui->setupUi(this);

    model = new QStandardItemModel(1, 8, this);
    setTabelHeader();
    viewAll();
}

void EmployeeManagementControl::setTabelHeader(){
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Employee ID")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Name")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Age")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Role Salary")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("Role ID")));
    model->setHorizontalHeaderItem(5, new QStandardItem(QString("Role")));
    model->setHorizontalHeaderItem(6, new QStandardItem(QString("Job Type")));
    model->setHorizontalHeaderItem(7, new QStandardItem(QString("Job Title")));
}

void EmployeeManagementControl::selectAll(){
    QSqlQuery query;
    if(!query.exec(AC.viewAndSaveEmp()))
        qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
    QStandardItem *item;
    int i = 0, j = 0;
    model->clear();
    setTabelHeader();
    while (query.next()) {
        j = 0;
        //emc.createEmp(query.value(0).toInt(), query.value(1).toString(), query.value(2).toInt(), query.value(8).toInt(), query.value(5).toString(), query.value(6).toString(), query.value(7).toString());
        item = new QStandardItem(QString(query.value(0).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(1).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(2).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(3).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(4).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(5).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(6).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(7).toString()));
        model->setItem(i, j++, item);
        i++;
    }

    ui->tableView->setModel(model);
}

EmployeeManagementControl::~EmployeeManagementControl()
{
    delete ui;
}


void EmployeeManagementControl::on_pushButton_3_clicked()
{
    QSqlQuery query;
    QString eid = ui->lineEdit_2->text();
    query.exec("select count(*) from employees where id = " + eid);
    query.next();
    if(query.value(0).toInt() < 1){
        QMessageBox::information(this, "not exist", "employees ID: " + eid + " not exist");
        return;
    }
    addRole role;
    role.setId(eid.toInt());
    role.setModal(true);
    role.exec();
}

void EmployeeManagementControl::viewAll(){
    QSqlQuery query;
    if(!query.exec(AC.viewAndSaveAll()))
        qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
    QStandardItem *item;
    int i = 0, j = 0;
    model->clear();
    setTabelHeader();
    while (query.next()) {
        j = 0;
        item = new QStandardItem(QString(query.value(0).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(1).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(2).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(3).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(4).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(5).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(6).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(7).toString()));
        model->setItem(i, j++, item);
        i++;
    }

    ui->tableView->setModel(model);

}

void EmployeeManagementControl::on_pushButton_7_clicked()
{
    QSqlQuery query;
    if(!query.exec(AC.viewAndSaveR()))
        qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
    QStandardItem *item;
    int i = 0, j = 0;
    model->clear();
    setTabelHeader();
    while (query.next()) {
        j = 0;
        item = new QStandardItem(QString(query.value(0).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(1).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(2).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(3).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(4).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(5).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(6).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(7).toString()));
        model->setItem(i, j++, item);
        i++;
    }

    ui->tableView->setModel(model);
}

void EmployeeManagementControl::on_pushButton_5_clicked()
{
    QSqlQuery query;
    QString q;
    if(ui->comboBox->currentText() == "Faculty")
        q = "select employees.id, employees.name, employees.age, roles.jobsalary, roles.id, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id = roles.eid and roles.type = 'faculty' group by roles.id;";
    if(ui->comboBox->currentText() == "Stuff")
        q = "select employees.id, employees.name, employees.age, roles.jobsalary, roles.id, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id = roles.eid and roles.type = 'stuff' group by roles.id;";
    if(ui->comboBox->currentText() == "TA")
        q = "select employees.id, employees.name, employees.age, roles.jobsalary, roles.id, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id = roles.eid and roles.type = 'ta' group by roles.id;";
    if(ui->comboBox->currentText() == "RA")
        q = "select employees.id, employees.name, employees.age, roles.jobsalary, roles.id, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id = roles.eid and roles.type = 'ra' group by roles.id;";

    if(!query.exec(q)){
        qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
    }
    QStandardItem *item;
    int i = 0, j = 0;
    model->clear();
    setTabelHeader();
    while (query.next()) {
        j = 0;
        item = new QStandardItem(QString(query.value(0).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(1).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(2).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(3).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(4).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(5).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(6).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(7).toString()));
        model->setItem(i, j++, item);
        i++;
    }

    ui->tableView->setModel(model);
}

void EmployeeManagementControl::on_pushButton_6_clicked()
{
    QSqlQuery query;
    QString q;
    if(ui->comboBox_2->currentText() == "FullTime")
        q = "select employees.id, employees.name, employees.age, roles.jobsalary, roles.id, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id = roles.eid and roles.jobtitle = 'fulltime' group by roles.id;";
    if(ui->comboBox_2->currentText() == "PartTime")
        q = "select employees.id, employees.name, employees.age, roles.jobsalary, roles.id, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id = roles.eid and roles.jobtitle = 'parttime' group by roles.id;";
    if(ui->comboBox_2->currentText() == "Term")
        q = "select employees.id, employees.name, employees.age, roles.jobsalary, roles.id, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id = roles.eid and roles.jobduration = 'term' group by roles.id;";
    if(ui->comboBox_2->currentText() == "Continuing")
        q = "select employees.id, employees.name, employees.age, roles.jobsalary, roles.id, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id = roles.eid and roles.jobduration = 'continuing' group by roles.id;";

    if(!query.exec(q)){
        qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
    }
    QStandardItem *item;
    int i = 0, j = 0;
    model->clear();
    setTabelHeader();
    while (query.next()) {
        j = 0;
        item = new QStandardItem(QString(query.value(0).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(1).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(2).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(3).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(4).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(5).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(6).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(7).toString()));
        model->setItem(i, j++, item);
        i++;
    }

    ui->tableView->setModel(model);
}

void EmployeeManagementControl::on_pushButton_4_clicked()
{
    QString eid = ui->lineEdit_3->text();
    QSqlQuery query;
    if(!query.exec("delete from employees where id=" + eid)){
        QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
    }
    if(!query.exec("delete from roles where eid=" + eid)){
        QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
    }
    if(!query.exec("delete from payments where eid=" + eid)){
        QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
    }
    QMessageBox::information(this, "success", "completed, click 'save' to see result!");
}

void EmployeeManagementControl::on_pushButton_clicked()
{
    newEmp np;
    np.setModal(true);
    np.exec();
}

void EmployeeManagementControl::on_pushButton_2_clicked()
{
    QSqlQuery query;
    QString eid = ui->lineEdit->text();
    query.exec("select count(*) from employees where id = " + eid);
    query.next();
    if(query.value(0).toInt() < 1){
        QMessageBox::information(this, "not exist", "employees ID: " + eid + " not exist");
        return;
    }
    editEmployee emp(0, eid.toInt());
    emp.setId(eid.toInt());
    emp.setModal(true);
    emp.exec();
}

void EmployeeManagementControl::on_pushButton_8_clicked()
{
    setTabelHeader();
    selectAll();
}

void EmployeeManagementControl::on_pushButton_9_clicked()
{
    QString rid = ui->lineEdit_4->text();
    QSqlQuery query;
    if(!query.exec("delete from roles where id=" + rid)){
        QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
    }else{
        QMessageBox::information(this, "success", "removed the role, click 'save' to see result!");
    }
}

void EmployeeManagementControl::on_pushButton_10_clicked()
{
    this->hide();
    PSOption ps;
    ps.setModal(true);
    ps.exec();
}

void EmployeeManagementControl::on_pushButton_12_clicked()
{
    QString amount = ui->lineEdit_5->text();
    QString percent = ui->lineEdit_6->text();
    QString type = ui->comboBox_3->currentText();
    QString title = ui->comboBox_4->currentText();
    QSqlQuery query;
    if(amount != "0"){
        if(!query.exec("update roles set jobsalary=jobsalary+"+amount+", raise=" + amount + " where type='" + type + "' and jobduration='" + title + "';")){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }else{
            QMessageBox::information(this, "success", "completed, click 'save' to see result!");
        }
    }else if(percent != "0"){
        float per = percent.toFloat()/100.0;
        QString q = "update roles set raise=jobsalary * " + QString::number(per) + ", jobsalary=jobsalary + (jobsalary * "+QString::number(per)+") where type='" + type + "' and jobduration='" + title + "';";
        //qDebug() << q;
        if(!query.exec(q)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }else{
            QMessageBox::information(this, "success", "completed, click 'save' to see result!");
        }
    }else{
        QMessageBox::information(this, "warnning", "invaild input");
    }
}

void EmployeeManagementControl::on_pushButton_11_clicked()
{
    AC.save(myqsql);
    viewAll();
}
