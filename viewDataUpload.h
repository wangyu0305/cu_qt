#ifndef DIT_H
#define DIT_H
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include <QDialog>
#include "accesscontrol.h"
#include <QStandardItemModel>
#include "employeecontrol.h"

namespace Ui {
class viewDataUpload;
}

class viewDataUpload : public QDialog
{
    Q_OBJECT

public:
    explicit viewDataUpload(QWidget *parent = 0);
    void uploadData();
    void setTabelHeader();
    void validation();
    void sync();
    void sync(QStringList);
    bool check(QStringList);
    ~viewDataUpload();

private slots:
    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::viewDataUpload *ui;
    accessControl AC;
    QStandardItemModel* model;
    employeeControl emc;
    int total;
    bool loaded;
};

#endif // DIT_H
