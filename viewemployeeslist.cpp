#include "viewemployeeslist.h"
#include "ui_employees.h"
#include <QStandardItemModel>
#include <QSqlQuery>
#include <QMessageBox>
#include <QDebug>
#include <QSqlError>
#include "employee.h"
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
viewEmployeesList::viewEmployeesList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::viewEmployeesList)
{
    ui->setupUi(this);
    model = new QStandardItemModel(1, 11, this);
    setTableHeader();
}

viewEmployeesList::~viewEmployeesList()
{
    delete ui;
}

void viewEmployeesList::on_pushButton_clicked()
{
    select();
}

void viewEmployeesList::setTableHeader(){
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Payment ID")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Employee ID")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Role")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Job Type")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("Job Title")));
    model->setHorizontalHeaderItem(5, new QStandardItem(QString("GrossIncome")));
    model->setHorizontalHeaderItem(6, new QStandardItem(QString("Deduction")));
    model->setHorizontalHeaderItem(7, new QStandardItem(QString("Raise")));
    model->setHorizontalHeaderItem(8, new QStandardItem(QString("NetIncome")));
    model->setHorizontalHeaderItem(9, new QStandardItem(QString("Start")));
    model->setHorizontalHeaderItem(10, new QStandardItem(QString("End")));
}

void viewEmployeesList::select(){
    model->clear();
    setTableHeader();
    QSqlQuery query;
    QString eid = ui->lineEdit->text();

    int year = ui->dateEdit->date().year();
    int month = ui->dateEdit->date().month();
    int day = ui->dateEdit->date().day();
    int endyear = ui->dateEdit_2->date().year();
    int endmonth = ui->dateEdit_2->date().month();
    int endday = ui->dateEdit->date().day();
    query.exec("select count(*) from employees where id = " + eid);
    query.next();
    if(query.value(0).toInt() < 1){
        QMessageBox::information(this, "not exist", "employees ID: " + eid + " not exist");
        return;
    }
    if(endyear < year || (endyear == year && endmonth < month)){
        QMessageBox::information(this, "error", "date incorrect, select again please!");
        return;
    }
    QString sdate = QString::number(year) + "-" + QString::number(month) + "-" + QString::number(day);
    QString edate = QString::number(endyear) + "-" + QString::number(endmonth) + "-" + QString::number(endday);

    QString q = "select payments.id, payments.eid, roles.type, roles.jobtitle, roles.jobduration, payments.grossincome, payments.deduction, payments.raise, payments.amount, payments.startdate, payments.enddate from payments, employees, roles "
                " where roles.id = payments.rid "
                " and payments.eid = " + eid + " and payments.startdate >= '" + sdate + "' and payments.enddate <='" + edate + "' group by payments.id;";

    //qDebug() << q;
    if(!query.exec(q))
        qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
    QStandardItem *item;
    int i = 0, j = 0;
    while (query.next()) {
        j = 0;
        item = new QStandardItem(QString(query.value(0).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(1).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(2).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(3).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(4).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(5).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(6).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(7).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(8).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(9).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(10).toString()));
        model->setItem(i, j++, item);
        i++;
    }

    ui->tableView->setModel(model);
}
