#-------------------------------------------------
#
# Project created by QtCreator 2017-01-11T16:56:21
#
#-------------------------------------------------

QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CuNics
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    addrole.cpp \
    editemployee.cpp \
    newemp.cpp \
    viewpayrollsummarynoticy.cpp \
    viewDataUpload.cpp \
    psoption.cpp \
    viewpayrollgenerateform.cpp \
    employeemanagementcontrol.cpp \
    viewemployeeslist.cpp \
    viewtransactiondata.cpp \
    employeecontrol.cpp \
    accesscontrol.cpp \
    ditcontrol.cpp

HEADERS  += mainwindow.h \
    defs.h \
    term.h \
    continuing.h \
    roles.h \
    payment.h \
    faculty.h \
    stuff.h \
    ta.h \
    ra.h \
    employee.h \
    addrole.h \
    editemployee.h \
    newemp.h \
    viewpayrollsummarynoticy.h \
    viewDataUpload.h \
    psoption.h \
    viewpayrollgenerateform.h \
    employeemanagementcontrol.h \
    viewemployeeslist.h \
    jobperiod.h \
    jobtype.h \
    fulltime.h \
    parttime.h \
    viewtransactiondata.h \
    employeecontrol.h \
    accesscontrol.h \
    ditcontrol.h

FORMS    += mainwindow.ui \
    ps.ui \
    employees.ui \
    em.ui \
    pgt.ui \
    dit.ui \
    addrole.ui \
    editemployee.ui \
    newemp.ui \
    viewpayrollsummarynoticy.ui \
    viewtransactiondata.ui

DISTFILES += \
    compile.sh \
    README.txt
