#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include <QMessageBox>
#include "psoption.h"
#include "viewemployeeslist.h"
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    //this->hide();
    if(ui->lineEdit->text() != "admin"){
        QMessageBox::information(this, "warnning", "input 'admin'");
    }else{
        PSOption pswindow;
        pswindow.setModal(true);
        pswindow.exec();
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    //this->hide();
    viewEmployeesList empwindow;
    empwindow.setModal(true);
    empwindow.exec();
}
