#include "psoption.h"
#include "ui_ps.h"
#include "employeemanagementcontrol.h"
#include "viewpayrollgenerateform.h"
#include "viewDataUpload.h"
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
PSOption::PSOption(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PSOption)
{
    ui->setupUi(this);
}

PSOption::~PSOption()
{
    delete ui;
}

void PSOption::on_pushButton_3_clicked()
{
    this->hide();
    EmployeeManagementControl employeeManagement;
    employeeManagement.setModal(true);
    employeeManagement.exec();
}

void PSOption::on_pushButton_clicked()
{
    this->hide();
    viewPayrollGenerateForm payroll;
    payroll.setModal(true);
    payroll.exec();
}

void PSOption::on_pushButton_2_clicked()
{
    this->hide();
    viewDataUpload dataIntegrationTool;
    dataIntegrationTool.setModal(true);
    dataIntegrationTool.exec();
}

