#ifndef PAYMENT
#define PAYMENT
#include<QString>
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
class Payment{

private:
    int pid;
    int eid;
    int rid;
    float grossIncome;
    float deduction;
    float raise;
    float amount;
    float netincome;
    QString date;
    QString sdate;
    QString edate;

public:
    Payment(){
        grossIncome = 0;
        deduction = 0;
        raise = 0;
        amount = 0;
        netincome = 0;
    }

    Payment(int pid, int eid, float g, float d, float r, QString date){
        this->pid = pid;
        this->eid = eid;
        this->grossIncome = g;
        this->deduction = d;
        this->raise = r;
        this->amount = grossIncome + raise - deduction;
        this->date = date;
    }
    Payment(int pid, int eid, int rid, float s, float d, float r, QString sdate, QString edate){
        this->pid = pid;
        this->eid = eid;
        this->rid = rid;
        this->amount = s;
        this->deduction = d;
        this->raise = r;
        this->sdate = sdate;
        this->edate = edate;
        this->netincome = s + r - d;
    }

    void setPayment(int pid, int eid, int rid, float s, float d, float r, QString sdate, QString edate){
        this->pid = pid;
        this->eid = eid;
        this->rid = rid;
        this->amount = s;
        this->deduction = d;
        this->raise = r;
        this->sdate = sdate;
        this->edate = edate;
        this->netincome = s + r - d;
    }

    int getPid(){return pid;}
    int getEid(){return eid;}
    float getGrossIncome(){return grossIncome;}
    float getDeduction(){return deduction;}
    float getRaise(){return raise;}
    float getAmount(){return amount;}
    void setAmount(float amount){
        this->amount = amount;
    }

    QString getDate(){return date;}
    QString paymentString(){
        return "insert into paymentsTrans values(" + QString::number(pid) + ", " + QString::number(eid) + ", " + QString::number(rid) + ", " + QString::number(amount) + ", " + QString::number(deduction) + ", " + QString::number(raise) + ", " + QString::number(netincome) +
                ", '" + sdate + "', '" + edate +"')";
    }

};

#endif // PAYMENT

