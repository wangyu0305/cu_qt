#ifndef TA
#define TA
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include "term.h"
#include "parttime.h"
#include "roles.h"

class TAs: public Roles{

private:
    JobType type;
    PartTime pt;
    float salary;

public:
    TAs(int r, int e, JobType te, PartTime p):Roles(r, e) ,type(te), pt(p){}
    TAs(int r, int e, float s):Roles(r, e), salary(s){}

    void setTAs(int r, int e, float s){
        eid = e;
        rid = r;
        salary = s;
    }

    Payment findPay(int pid){
        for(int i = 0; i < counter; i++){
            if(pid == myPay[i].getPid()){
                return myPay[i];
            }
        }
    }

    Payment findPayByDate(QString date){
        for(int i = 0; i < counter; i++){
            if(date == myPay[i].getDate()){
                return myPay[i];
            }
        }
    }

    void addPay(Payment pay){ myPay[counter++] = pay;}
    int getId(){return rid;}
    int getEid(){return eid;}
    void correctincome(float amount){
        type.setSalary(amount);
    }
    void deduction(float amount){
        type.setSalary(type.getSalary() - amount);
    }
    void deductionPercent(int per){
        type.setSalary(type.getSalary() - (type.getSalary()/ (per)));
    }

    QString TAString(){
        return "insert into rolesTrans values(" + QString::number(rid) + ", " + QString::number(eid) + ", 'ta'" + ", 'parttime'" + ", 'term'" + ", " + QString::number(salary) + ", 0)";
    }
};

#endif // TA

