#ifndef PARTTIME
#define PARTTIME
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include "jobperiod.h"
#include "defs.h"

class PartTime: JobPeriod{
private:
    JobTitleType type;

public:
    JobTitleType getType(){return type;}
    string getTypeString(){return "parttime";}
};

#endif // PARTTIME

