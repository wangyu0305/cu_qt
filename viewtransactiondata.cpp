#include "viewtransactiondata.h"
#include "ui_viewtransactiondata.h"

#include <QStandardItemModel>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>

/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/

viewTransactionData::viewTransactionData(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::viewTransactionData)
{
    ui->setupUi(this);
    viewData();
}

viewTransactionData::~viewTransactionData()
{
    delete ui;
}

void viewTransactionData::viewData(){
    QStandardItemModel* model = new QStandardItemModel(1, 11, this);

    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Employee ID")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Name")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Age")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Role ID")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("Role Salary")));
    model->setHorizontalHeaderItem(5, new QStandardItem(QString("Role")));
    model->setHorizontalHeaderItem(6, new QStandardItem(QString("Job Type")));
    model->setHorizontalHeaderItem(7, new QStandardItem(QString("Job Title")));
    model->setHorizontalHeaderItem(8, new QStandardItem(QString("income")));
    model->setHorizontalHeaderItem(9, new QStandardItem(QString("raise")));
    model->setHorizontalHeaderItem(10, new QStandardItem(QString("deduction")));

    QSqlQuery query;
    QStandardItem *item;
    int i = 0, j = 0;

    if(!query.exec("select em.id, em.name, em.age, rol.id, rol.jobsalary, rol.type, rol.jobtitle, rol.jobduration, sum(pay.amount), (pay.raise+rol.raise), sum(pay.deduction) from payments as pay, employees as em, roles as rol where pay.eid = em.id and em.id = rol.eid group by rol.id order by em.id;"))
        qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();

    while (query.next()) {
        j = 0;
        item = new QStandardItem(QString(query.value(0).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(1).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(2).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(3).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(4).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(5).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(6).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(7).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(8).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(9).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(10).toString()));
        model->setItem(i, j++, item);
        i++;
    }

    ui->tableView->setModel(model);
}

void viewTransactionData::on_pushButton_clicked()
{
    this->hide();
}
