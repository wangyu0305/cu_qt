#ifndef VIEWALL_H
#define VIEWALL_H

#include <QDialog>

namespace Ui {
class ViewAll;
}

class ViewAll : public QDialog
{
    Q_OBJECT

public:
    explicit ViewAll(QWidget *parent = 0);
    ~ViewAll();

private:
    Ui::ViewAll *ui;
};

#endif // VIEWALL_H
