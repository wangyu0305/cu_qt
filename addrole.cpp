#include "addrole.h"
#include "ui_addrole.h"
#include <QMessageBox>
#include "employeemanagementcontrol.h"
#include <QSqlQuery>
#include <QDateEdit>
#include <QDebug>
#include <QSqlError>
#include "employee.h"
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/

addRole::addRole(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addRole)
{
    ui->setupUi(this);
}

void addRole::setId(int eid){
    this->eid = eid;
}

addRole::~addRole()
{
    delete ui;
}

void addRole::on_buttonBox_accepted()
{
    QSqlQuery query;
    QString role = ui->comboBox->currentText();
    QString title = ui->comboBox_2->currentText();
    QString duration = ui->comboBox_3->currentText();
//    QString term = ui->comboBox_4->currentText();
    QString salary = ui->lineEdit->text();
//    int year = ui->dateEdit->date().year();
//    int month = ui->dateEdit->date().month();
//    int endyear = ui->dateEdit_2->date().year();
//    int endmonth = ui->dateEdit_2->date().month();
    //Employee em;
    if(role == "Faculty"){
        role = "faculty";
        QMessageBox::information(this, "error", "faculty can't fit other role, select again please!");
        return;
    }
    else if(role == "Staff")
        role = "staff";
    else if(role == "TA")
        role = "ta";
    else
        role = "ra";
    if(title == "FullTime")
        title = "fulltime";
    else
        title = "parttime";
    if(duration == "Term")
        duration = "term";
    else
        duration = "continuing";
//    if(term == "Winter")
//        term = "winter";
//    else if(term == "Spring")
//        term = "spring";
//    else if(term == "Summer")
//        term == "summer";
//    else if(term == "Fall")
//        term == "fall";
//    else
//        term = "";
    if(role == "faculty" && title != "fulltime"){
        QMessageBox::information(this, "error", "faculty can't be parttime, select again please!");
        return;
    }
    if((role == "ta" || role == "ra") && title != "parttime" && duration != "term"){
        QMessageBox::information(this, "error", "ta and ra must be parttime and term, select again please!");
        return;
    }
//    if(endyear < year || (endyear == year && endmonth < month)){
//        QMessageBox::information(this, "error", "date incorrect, select again please!");
//        return;
//    }
    if(salary.toFloat() <= 0){
        QMessageBox::information(this, "error", "salary incorrect, input int or float again please!");
        return;
    }
//    QString sdate = QString::number(year) + "-" + QString::number(month);
//    QString edate = QString::number(endyear) + "-" + QString::number(endmonth);
    query.exec("SELECT max(id) from roles;");
    query.next();
    int rid = query.value(0).toInt() + 1;
    QString q = "insert into roles values("+ QString::number(rid)+" , " + QString::number(eid) + ", '"+ role +"'" + ", '"+ title+"'" + ", '"+duration+"'" + ", " + salary + ", 0)";
    //qDebug()<<q;
    //em.addRole(role);
    if(!query.exec(q)){
        QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
    }else{
        QMessageBox::information(this, "success", "completed, click 'save' to see result!");
    }
    this->hide();
}

void addRole::on_buttonBox_rejected()
{
    this->hide();
//    Em em;
//    em.setModal(true);
//    em.exec();
}
