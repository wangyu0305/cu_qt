#ifndef EMPLOYEES_H
#define EMPLOYEES_H
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include <QDialog>
#include <QStandardItemModel>

namespace Ui {
class viewEmployeesList;
}

class viewEmployeesList : public QDialog
{
    Q_OBJECT

public:
    explicit viewEmployeesList(QWidget *parent = 0);
    ~viewEmployeesList();

private slots:
    void on_pushButton_clicked();
    void setTableHeader();
    void select();

private:
    Ui::viewEmployeesList *ui;
    QStandardItemModel *model;
};

#endif // EMPLOYEES_H
