#ifndef DEFS
#define DEFS
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/

#include <string>

using namespace std;

#define MAX_ARR  64

typedef enum { STUFF, FACULTY, TA, RA } RoleType;

typedef enum { WINTER, SPRING, SUMMER, FALL } SeasonType;

typedef enum { FULLTIME, PARTTIME } JobTitleType;

typedef enum { Term, Continuing } JobDurationType;

typedef enum { FAIL, SUCCESS} TransState;

typedef struct Date_T{
    int year;
    int month;
    int day;
}Date;

class IDS{

public:
    static int eid;
    static int rid;
    static int pid;

    static int getEID(){return eid++;}
    static int getRID(){return rid++;}
    static int getPID(){return pid++;}
};

int IDS::eid = 1001;
int IDS::rid = 101;
int IDS::pid = 11;

#endif // DEFS

