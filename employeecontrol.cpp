#include "employeecontrol.h"
#include <QVariant>

employeeControl::employeeControl()
{
    counter = 0;
    ems = new Employee*[100];
}

employeeControl::~employeeControl(){
    for(int i = 0; i < counter; i++){
        delete(ems[i]);
    }
    delete(ems);
}

void employeeControl::createEmp(int eid, QString name, int age, int rid, QString type, QString title, QString duration){
    ems[counter] = new Employee(eid, name, age);
    ems[counter]->addRole(new Roles(rid, eid, type, title, duration));
    counter++;
}

void employeeControl::removeEmp(int eid){
    for(int i = 0; i<counter; i++){
        if(ems[i]->getEID() == eid){
            for(int j = i+1; j < counter; j++){
                ems[i] = ems[j];
            }
            delete(ems[counter-1]);
            counter--;
        }
    }
}

void employeeControl::addRole(int rid, int eid, QString type, QString title, QString duration){
    for(int i = 0; i < counter; i++){
        if(ems[i]->getEID() == eid){
            ems[i]->addRole(new Roles(rid, eid, type, title, duration));
        }
    }
}

void employeeControl::modifyRole(int rid, int eid, QString type, QString title, QString duration){
    for(int i = 0; i < counter; i++){
        if(ems[i]->getEID() == eid){
            ems[i]->modifyRole(rid, type, title, duration);
        }
    }
}

void employeeControl::makePay(int pid, int eid, int rid, float s, float d, float r, QString sdate, QString edate){
    for(int i = 0; i < counter; i++){
        if(ems[i]->getEID() == eid){
            ems[i]->addPaytoRole(pid, eid, rid, s, d, r, sdate, edate);
        }
    }
}

void employeeControl::raise(int pid, int eid, int rid, float r){
    for(int i = 0; i < counter; i++){
        if(ems[i]->getEID() == eid){
            ems[i]->raiseToRole(pid, rid, r);
        }
    }
}

void employeeControl::raise(int pid, int eid, int rid, int r){
    for(int i = 0; i < counter; i++){
        if(ems[i]->getEID() == eid){
            ems[i]->raiseToRole(pid, rid, r);
        }
    }
}

void employeeControl::deduction(int pid, int eid, int rid, float d){
    for(int i = 0; i < counter; i++){
        if(ems[i]->getEID() == eid){
            ems[i]->deductionOnRole(pid, rid, d);
        }
    }
}

void employeeControl::deduction(int pid, int eid, int rid, int d){
    for(int i = 0; i < counter; i++){
        if(ems[i]->getEID() == eid){
            ems[i]->deductionOnRole(pid, rid, d);
        }
    }
}



void employeeControl::removeRole(int rid, int eid){
    for(int i = 0; i < counter; i++){
        if(ems[i]->getEID() == eid){
            ems[i]->removeRole(rid);
        }
    }
}

bool employeeControl::find(int eid){
    for(int i = 0; i < counter; i++){
        if(ems[i]->getEID() == eid){
            return true;
        }
    }
    return false;
}

bool employeeControl::existRole(int eid, int rid){
    for(int i = 0; i < counter; i++){
        if(ems[i]->getEID() == eid){
            return ems[i]->existRole(rid);
        }
    }
    return false;
}

void employeeControl::merge(QStringList list){
    try{
        for(int i = 0; i < counter; i++){
            if(ems[i]->getEID() == list.at(3).toInt()){
                if(ems[i]->getName() != list.at(4)){
                    ems[i]->setName(list.at(4));
                }
                if(ems[i]->getAge() != list.at(5).toInt()){
                    ems[i]->setAge(list.at(5).toInt());
                }
                if(list.at(1) == "update-addrole"){
                    this->addRole(i, list.at(10).toInt(), list.at(7), list.at(8), list.at(9));
                }
                if(list.at(1) == "update"){
                    ems[i]->modifyRole(list.at(10).toInt(), list.at(7), list.at(8), list.at(9));
                }
                if(list.at(1) == "update-addpay"){
                    //this->makePay(list.at(11).toInt(), list.at(3).toInt(), list.at(10).toInt(), list.at(12).toFloat(), list.at(13).toFloat(), list.at(14).toFloat(), list.at(15), list.at(16));
                }
                if(list.at(1) == "update-adjust"){
                    for(int i = 0; i < counter; i++){
                        if(ems[i]->getEID() == list.at(3).toInt()){
                            if(list.at(13).toFloat() != 0){
                                ems[i]->raiseToRole(list.at(11).toInt(), list.at(10).toInt(), list.at(13).toFloat());
                            }
                            if(list.at(14).toFloat() != 0){
                                ems[i]->deductionOnRole(list.at(11).toInt(), list.at(10).toInt(), list.at(14).toFloat());
                            }
                        }
                    }
                }
                if(list.at(1) == "add"){
                    this->createEmp(list.at(3).toInt(), list.at(4), list.at(5).toInt(), list.at(10).toInt(), list.at(7), list.at(8), list.at(9));
                }
            }
        }
    }catch(...){
        return ;
    }


}
