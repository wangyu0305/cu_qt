#include "viewpayrollgenerateform.h"
#include "ui_pgt.h"
#include <QSqlQuery>
#include <QMessageBox>
#include <QDebug>
#include <QSqlError>
#include "viewpayrollsummarynoticy.h"
#include "employee.h"
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
viewPayrollGenerateForm::viewPayrollGenerateForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::viewPayrollGenerateForm)
{
    ui->setupUi(this);
}

viewPayrollGenerateForm::~viewPayrollGenerateForm()
{
    delete ui;
}

void viewPayrollGenerateForm::on_pushButton_3_clicked()
{
    QString netincome = ui->lineEdit->text();
    QString percent = ui->lineEdit_2->text();
    QString amount = ui->lineEdit_4->text();
    int index = ui->comboBox->currentIndex();

    int year = ui->dateEdit->date().year();
    int month = ui->dateEdit->date().month();
    //int day = ui->dateEdit->date().day();
    int endyear = ui->dateEdit_2->date().year();
    int endmonth = ui->dateEdit_2->date().month();
    //int endday = ui->dateEdit->date().day();

    QSqlQuery query, insert;

    if(endyear < year || (endyear == year && endmonth <= month)){
        QMessageBox::information(this, "error", "date incorrect, select again please!");
        return;
    }
    query.exec("SELECT max(id) from payments;");
    query.next();
    int max = query.value(0).toInt();
    int pid = query.value(0).toInt() + 1;
    QString q;
    QString role;
    if(index == 1){
        role = "faculty";
    }else if(index == 2){
        role = "staff";
    }else if(index == 3){
        role = "ta";
    }else if(index == 4){
        role = "ra";
    }else{
        role = "";
    }
    if(netincome != "0"){
        q = "select roles.id, roles.eid, roles.raise from roles ";
    }else{
        q = "select roles.id, roles.eid, roles.raise, roles.jobsalary from roles ";
    }
    if(role != ""){
        q += "where roles.type='"+role+"'";
    }
    query.exec(q);
    int months = (endyear - year) * 12;
    if(endmonth > month)
        months += (endmonth - month);
    else if(endmonth < month)
        months -= (month - endmonth);
    //qDebug() << months;
    while(query.next()){
        int i = year;
        int j = month;
        int count = months;
        while (count-- > 0) {
            j++;
            if(j > 12){
                i++;
                j = 1;
            }
            float netsalary;
            float income;
            float deduc = 0.0;
            if(netincome == "0"){
                income = query.value(3).toFloat();
            }else{
                income = netincome.toFloat();
            }
            if(amount != "0"){
                netsalary = income - amount.toFloat();
                deduc = amount.toFloat();
            }else if(percent != "0"){
                netsalary = income - (income * (percent.toFloat()/100.0));
                deduc = income * percent.toFloat();
            }else {
                netsalary = income;
            }
            QString sdate = QString::number(i) + "-" + QString::number(j-1) + "-" + QString::number(1);
            QString edate = QString::number(i) + "-" + QString::number(j) + "-" + QString::number(1);
            q = "insert into payments values("+ QString::number(pid++) +"," + query.value(1).toString() + "," +query.value(0).toString() + "," + QString::number(netsalary)+ "," + QString::number(deduc) + "," + query.value(2).toString() + "," + QString::number(income) + ",'" + sdate + "','" + edate + "')";
            //qDebug() << q;
            if(!insert.exec(q)){
                qDebug() << "SqLite error insert:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
            }
        }
    }
    this->hide();
    ViewPayrollSummaryNoticy psn(0, max);
    psn.setModal(true);
    psn.exec();
}

void viewPayrollGenerateForm::on_pushButton_4_clicked()
{

    QString netincome = ui->lineEdit->text();
    QString percent = ui->lineEdit_2->text();
    QString amount = ui->lineEdit_4->text();
    QString eid = ui->lineEdit_5->text();
    QString rid = ui->lineEdit_6->text();

    int year = ui->dateEdit->date().year();
    int month = ui->dateEdit->date().month();
    //int day = ui->dateEdit->date().day();
    int endyear = ui->dateEdit_2->date().year();
    int endmonth = ui->dateEdit_2->date().month();
    //int endday = ui->dateEdit->date().day();

    QSqlQuery query, insert;
    query.exec("SELECT max(id) from payments;");
    query.next();
    int max = query.value(0).toInt();
    int pid = query.value(0).toInt() + 1;
    QString q;
    qDebug() << rid;
    if(endyear < year || (endyear == year && endmonth <= month)){
        QMessageBox::information(this, "error", "date incorrect, select again please!");
        return;
    }
    int months = (endyear - year) * 12;
    if(endmonth > month)
        months += (endmonth - month);
    else if(endmonth < month)
        months -= (month - endmonth);
    int i = year;
    int j = month;
    int count = months;
    while (count-- > 0) {
        j++;
        if(j > 12){
            i++;
            j = 1;
        }

    float netsalary;
    float income;
    float deduc = 0.0;
    if(netincome == "0"){
        income = 5000;
    }else{
        income = netincome.toFloat();
    }
    if(amount != "0"){
        netsalary = income - amount.toFloat();
        deduc = amount.toFloat();
    }else if(percent != "0"){
        netsalary = income - (income * (percent.toFloat()/100.0));
        deduc = income * percent.toFloat();
    }else {
        netsalary = income;
    }
    QString sdate = QString::number(i) + "-" + QString::number(j-1) + "-" + QString::number(1);
    QString edate = QString::number(i) + "-" + QString::number(j) + "-" + QString::number(1);
    q = "insert into payments values("+ QString::number(pid++) +"," + eid + "," + rid + "," + QString::number(netsalary)+ "," + QString::number(deduc) + "," + "0" + "," + QString::number(income) + ",'" + sdate + "','" + edate + "')";
    //qDebug() << q;
    if(!insert.exec(q)){
        qDebug() << "SqLite error insert:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
    }
    }
    ViewPayrollSummaryNoticy psn(0, max);
    psn.setModal(true);
    psn.exec();
}

void viewPayrollGenerateForm::on_pushButton_5_clicked()
{
    this->hide();
}
