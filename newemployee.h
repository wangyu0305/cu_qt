#ifndef NEWEMPLOYEE_H
#define NEWEMPLOYEE_H

#include <QDialog>

namespace Ui {
class newemployee;
}

class newemployee : public QDialog
{
    Q_OBJECT

public:
    explicit newemployee(QWidget *parent = 0);
    ~newemployee();

private:
    Ui::newemployee *ui;
};

#endif // NEWEMPLOYEE_H
