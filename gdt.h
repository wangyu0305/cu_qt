#ifndef GDT_H
#define GDT_H

#include <QDialog>

namespace Ui {
class GDT;
}

class GDT : public QDialog
{
    Q_OBJECT

public:
    explicit GDT(QWidget *parent = 0);
    ~GDT();

private:
    Ui::GDT *ui;
};

#endif // GDT_H
