#include "newemp.h"
#include "ui_newemp.h"
#include <QSqlQuery>
#include <QMessageBox>
#include <QDebug>
#include <QSqlError>
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
newEmp::newEmp(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::newEmp)
{
    ui->setupUi(this);
}

newEmp::~newEmp()
{
    delete ui;
}

void newEmp::on_buttonBox_accepted()
{

    QSqlQuery query;
    QString name = ui->lineEdit_2->text();
    QString age = ui->lineEdit_3->text();
    QString role = ui->comboBox->currentText();
    QString title = ui->comboBox_2->currentText();
    QString duration = ui->comboBox_3->currentText();
//    QString term = ui->comboBox_4->currentText();
    QString salary = ui->lineEdit->text();
//    int year = ui->dateEdit->date().year();
//    int month = ui->dateEdit->date().month();
//    int endyear = ui->dateEdit_2->date().year();
//    int endmonth = ui->dateEdit_2->date().month();
    if(role == "Faculty")
        role = "faculty";
    else if(role == "Staff")
        role = "staff";
    else if(role == "TA")
        role = "ta";
    else
        role = "ra";
    if(title == "FullTime")
        title = "fulltime";
    else
        title = "parttime";
    if(duration == "Term")
        duration = "term";
    else
        duration = "continuing";
//    if(term == "Winter")
//        term = "winter";
//    else if(term == "Spring")
//        term = "spring";
//    else if(term == "Summer")
//        term == "summer";
//    else if(term == "Fall")
//        term == "fall";
//    else
//        term = "";
    if(role == "faculty" && title != "fulltime"){
        QMessageBox::information(this, "error", "faculty can't be parttime, select again please!");
        return;
    }
    if((role == "ta" || role == "ra") && title != "parttime" && duration != "term"){
        QMessageBox::information(this, "error", "ta and ra must be parttime and term, select again please!");
        return;
    }
//    if(endyear < year || (endyear == year && endmonth < month)){
//        QMessageBox::information(this, "error", "date incorrect, select again please!");
//        return;
//    }
    if(salary.toFloat() <= 0){
        QMessageBox::information(this, "error", "salary incorrect, input int or float again please!");
        return;
    }
//    QString sdate = QString::number(year) + "-" + QString::number(month);
//    QString edate = QString::number(endyear) + "-" + QString::number(endmonth);
    query.exec("SELECT max(id) from employees;");
    query.next();
    int eid = query.value(0).toInt() + 1;
    query.exec("SELECT max(id) from roles;");
    query.next();
    int rid = query.value(0).toInt() + 1;
    QString r = "insert into employees values(" + QString::number(eid) + ", '" + name + "', " + age + ")";
    QString q = "insert into roles values("+ QString::number(rid)+" , " + QString::number(eid) + ", '"+ role +"'" + ", '"+ title+"'" + ", '"+duration+"'" + ", " + salary + ", 0)";
    //qDebug()<<q;
    if(!query.exec(r)){
        QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
    }else{
        //QMessageBox::information(this, "success", "completed, click 'view all roles' to see result!");
    }

    if(!query.exec(q)){
        QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
    }else{
        QMessageBox::information(this, "success", "completed, click 'view all roles' to see result!");
    }
    this->hide();
}
