#include "viewDataUpload.h"
#include "ui_dit.h"
#include "viewtransactiondata.h"
#include <QSqlQuery>
#include <QDebug>
#include <QMessageBox>
#include <QSqlError>
#include <QFile>
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
viewDataUpload::viewDataUpload(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::viewDataUpload)
{
    ui->setupUi(this);
    model = new QStandardItemModel(1, 17, this);
    setTabelHeader();
    total = 0;
    loaded = false;
}

void viewDataUpload::setTabelHeader(){
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Transaction ID")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Type")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Date")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Employee ID")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("Name")));
    model->setHorizontalHeaderItem(5, new QStandardItem(QString("Age")));
    model->setHorizontalHeaderItem(6, new QStandardItem(QString("Role Salary")));
    model->setHorizontalHeaderItem(7, new QStandardItem(QString("Role")));
    model->setHorizontalHeaderItem(8, new QStandardItem(QString("Job Type")));
    model->setHorizontalHeaderItem(9, new QStandardItem(QString("Job Title")));
    model->setHorizontalHeaderItem(10, new QStandardItem(QString("Role ID")));
    model->setHorizontalHeaderItem(11, new QStandardItem(QString("Payment ID")));
    model->setHorizontalHeaderItem(12, new QStandardItem(QString("income")));
    model->setHorizontalHeaderItem(13, new QStandardItem(QString("raise")));
    model->setHorizontalHeaderItem(14, new QStandardItem(QString("deduction")));
    model->setHorizontalHeaderItem(15, new QStandardItem(QString("S-date")));
    model->setHorizontalHeaderItem(16, new QStandardItem(QString("E-date")));

    ui->tableView->setModel(model);
}

viewDataUpload::~viewDataUpload()
{
    delete ui;
}

void viewDataUpload::on_pushButton_3_clicked()
{
    viewTransactionData td;
    td.setModal(true);
    td.exec();
}

void viewDataUpload::uploadData(){
    QSqlQuery query;
    QStringList list;
    setTabelHeader();
    QFile inputFile("transactions.txt");
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       QStandardItem *item;
       int i = 0, j = 0;
       while (!in.atEnd())
       {
          QString line = in.readLine();
          list = line.split(",");
              j = 0;
              item = new QStandardItem(list.at(0));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(1));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(2));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(3));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(4));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(5));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(6));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(7));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(8));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(9));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(10));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(11));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(12));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(13));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(14));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(15));
              model->setItem(i, j++, item);
              item = new QStandardItem(list.at(16));
              model->setItem(i, j++, item);
//              query.exec(AC.createTrans(QString::number(list.at(0)), QString::number(list.at(3)), QString::number(list.at(10)), list.at(1)));
//              query.exec(AC.tEmployeeSql(list.at(1).toInt()), list.at(4), list.at(5).toInt());
//              query.exec(AC.tRoleSql(list.at(10).toInt(), list.at(3).toInt(), list.at(7), list.at(8), list.at(9), list.at(6)));
//              if(list.at(11).toInt() != 0){
//                  query.exec(AC.tPaymentSql(list.at(11).toInt(), list.at(3).toInt(), list.at(10).toInt(), list.at(12).toFloat(), list.at(13).toFloat(), list.at(14).toFloat(), list.at(12).toFloat(), list.at(15), list.at(16)));

//              }
              i++;
          //}
          //qDebug() << list.at(0);
       }
       total = i;
       ui->tableView->setModel(model);
       inputFile.close();
    }else{
        QMessageBox::information(this, "error", "file error: not find!");
    }
}

void viewDataUpload::on_pushButton_2_clicked()
{
    uploadData();
    return;


    QMessageBox::information(this, "success", "completed, click 'employee management' to see result!");
}

void viewDataUpload::sync(){
    QSqlQuery query;
    if(!query.exec("select employees.id, employees.name, employees.age, sum(roles.jobsalary), count(roles.id), roles.type, roles.jobtitle, roles.jobduration, roles.id from employees, roles where employees.id = roles.eid group by employees.id;"))
        qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
    while(query.next()){
        emc.createEmp(query.value(0).toInt(), query.value(1).toString(), query.value(2).toInt(), query.value(8).toInt(), query.value(5).toString(), query.value(6).toString(), query.value(7).toString());
    }
}

void viewDataUpload::sync(QStringList list){
    //qDebug() << list.at(0) << list.at(1) << list.at(2) << list.at(3) << list.at(4) << list.at(5) << list.at(6) << list.at(7) << list.at(8) << list.at(9) << list.at(10);
    QSqlQuery query;
    if(list.at(1) == "add"){
        //qDebug() << AC.EmployeeSql(list.at(3).toInt(), list.at(4), list.at(5).toInt());
        query.exec(AC.EmployeeSql(list.at(3).toInt(), list.at(4), list.at(5).toInt()));
        query.exec(AC.RoleSql(list.at(10).toInt(), list.at(3).toInt(), list.at(7), list.at(8), list.at(9), list.at(6).toInt()));
        if(list.at(11).toInt() != 0){
            //qDebug() << AC.PaymentSql(list.at(11).toInt(), list.at(3).toInt(), list.at(10).toInt(), list.at(12).toFloat(), list.at(13).toFloat(), list.at(14).toFloat(), list.at(12).toFloat(), list.at(15), list.at(16));
               query.exec(AC.PaymentSql(list.at(11).toInt(), list.at(3).toInt(), list.at(10).toInt(), list.at(12).toFloat(), list.at(13).toFloat(), list.at(14).toFloat(), list.at(12).toFloat(), list.at(15), list.at(16)));
        }
    }else if(list.at(1) == "remove"){
        query.exec(AC.remove(list.at(3).toInt()));
        query.exec(AC.removeAllRole(list.at(3).toInt()));
        query.exec(AC.removeAllPayment(list.at(3).toInt()));
    }else if(list.at(1) == "update-addrole"){
        query.exec(AC.RoleSql(list.at(10).toInt(), list.at(3).toInt(), list.at(7), list.at(8), list.at(9), list.at(6).toInt()));
    }
    else if(list.at(1) == "remove-role"){
        query.exec(AC.removeRole(list.at(10).toInt()));
    }else if(list.at(1) == "update"){
        //qDebug() << AC.remove(list.at(3).toInt());
        query.exec(AC.remove(list.at(3).toInt()));
        query.exec(AC.removeAllRole(list.at(3).toInt()));
        query.exec(AC.removeAllPayment(list.at(3).toInt()));

        query.exec(AC.EmployeeSql(list.at(1).toInt(), list.at(4), list.at(5).toInt()));
        query.exec(AC.RoleSql(list.at(10).toInt(), list.at(3).toInt(), list.at(7), list.at(8), list.at(9), list.at(6).toInt()));
        if(list.at(11).toInt() != 0){
               query.exec(AC.PaymentSql(list.at(11).toInt(), list.at(3).toInt(), list.at(10).toInt(), list.at(12).toFloat(), list.at(13).toFloat(), list.at(14).toFloat(), list.at(12).toFloat(), list.at(15), list.at(16)));
        }
    }else if(list.at(1) == "update-addpay"){
        query.exec(AC.PaymentSql(list.at(11).toInt(), list.at(3).toInt(), list.at(10).toInt(), list.at(12).toFloat(), list.at(13).toFloat(), list.at(14).toFloat(), list.at(12).toFloat(), list.at(15), list.at(16)));
    }
    else if(list.at(1) == "update-adjust"){
        query.exec(AC.removeRole(list.at(10).toInt()));
        query.exec(AC.removePayment(list.at(11).toInt()));
        query.exec(AC.RoleSql(list.at(10).toInt(), list.at(3).toInt(), list.at(7), list.at(8), list.at(9), list.at(6).toInt()));
        if(list.at(11).toInt() != 0){
               query.exec(AC.PaymentSql(list.at(11).toInt(), list.at(3).toInt(), list.at(10).toInt(), list.at(12).toFloat(), list.at(14).toFloat(), list.at(13).toFloat(), -list.at(14).toFloat(), list.at(15), list.at(16)));
        }
    }
    else{
        //qDebug() << list.at(13).toInt();
        if(list.at(13).toInt() < 100){
            //qDebug() << AC.raisePer(list.at(10).toInt(), list.at(13).toInt());
            query.exec(AC.raisePer(list.at(10).toInt(), list.at(13).toInt()));
        }else{
            //qDebug() << AC.raise(list.at(10).toInt(), list.at(13).toInt());
            query.exec(AC.raise(list.at(10).toInt(), list.at(13).toInt()));
        }
    }

    //QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
}

bool viewDataUpload::check(QStringList list){
    if(list.at(1) != "remove"){
        if(emc.find(list.at(3).toInt())){
            if(emc.existRole(list.at(3).toInt(), list.at(10).toInt())){
                return true;
            }
        }
    }
    return true;
}

void viewDataUpload::validation(){
    if(total == 0){
        QMessageBox::information(this, "error", "please upload transaction file first");
        return ;
    }
    sync();
    QStandardItemModel *absmodel = model;
    QStringList list;

    for(int i = 0; i < absmodel->rowCount(); i++)
    {
        QModelIndex index = absmodel->index(i, 0);
        list.append(index.data().toString());
        index = absmodel->index(i, 1);
        list.append(index.data().toString());
        index = absmodel->index(i, 2);
        list.append(index.data().toString());
        index = absmodel->index(i, 3);
        list.append(index.data().toString());
        index = absmodel->index(i, 4);
        list.append(index.data().toString());
        index = absmodel->index(i, 5);
        list.append(index.data().toString());
        index = absmodel->index(i, 6);
        list.append(index.data().toString());
        index = absmodel->index(i, 7);
        list.append(index.data().toString());
        index = absmodel->index(i, 8);
        list.append(index.data().toString());
        index = absmodel->index(i, 9);
        list.append(index.data().toString());
        index = absmodel->index(i, 10);
        list.append(index.data().toString());
        index = absmodel->index(i, 11);
        list.append(index.data().toString());
        index = absmodel->index(i, 12);
        list.append(index.data().toString());
        index = absmodel->index(i, 13);
        list.append(index.data().toString());
        index = absmodel->index(i, 14);
        list.append(index.data().toString());
        index = absmodel->index(i, 15);
        list.append(index.data().toString());
        index = absmodel->index(i, 16);
        list.append(index.data().toString());
        if(!check(list)) continue;
        //qDebug() << list.at(0);
        //emc.merge(list);

        sync(list);
        list.clear();
    }
    QMessageBox::information(this, "success", "click 'view result' to view result:");
}

void viewDataUpload::on_pushButton_4_clicked()
{

    if(loaded){
        QMessageBox::information(this, "failure", "already merged! merge again is forbided!");
        return;
    }else{
        loaded = true;
    }
    validation();
    QSqlQuery query, queryex;
    QString eid;

    query.exec("select tr.eid from transactions as tr where type='remove'");

    return;
    while (query.next()) {
        eid = query.value(0).toString();
        if(!queryex.exec("delete from employees where id=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
        if(!queryex.exec("delete from roles where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
        if(!queryex.exec("delete from payments where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
    }

    query.exec("select eid from transactions where type='add';");
    while (query.next()) {
        eid = query.value(0).toString();
        if(!queryex.exec("insert into employees select * from employeesTrans where id=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
        if(!queryex.exec("insert into roles select * from rolesTrans where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
        if(!queryex.exec("insert into payments select * from paymentsTrans where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
    }

    query.exec("select eid from transactions where type='update-addrole';");
    while (query.next()) {
        eid = query.value(0).toString();
        if(!queryex.exec("insert into roles select * from rolesTrans where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
        if(!queryex.exec("insert into payments select * from paymentsTrans where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
    }

    query.exec("select eid from transactions where type='update-addpay';");
    while (query.next()) {
        eid = query.value(0).toString();
        if(!queryex.exec("insert into payments select * from paymentsTrans where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
    }

    query.exec("select eid from transactions where type='update-adjustpay';");
    while (query.next()) {
        eid = query.value(0).toString();
        if(!queryex.exec("delete from payments where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
        if(!queryex.exec("insert into payments select * from paymentsTrans where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
    }

    query.exec("select eid from transactions where type='update';");
    while (query.next()) {
        eid = query.value(0).toString();
        if(!queryex.exec("delete from employees where id=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
        if(!queryex.exec("insert into employees select * from employeesTrans where id=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
        if(!queryex.exec("delete from roles where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
        if(!queryex.exec("insert into roles select * from rolesTrans where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
        if(!queryex.exec("delete from payments where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
        if(!queryex.exec("insert into payments select * from paymentsTrans where eid=" + eid)){
            QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
        }
    }
}

void viewDataUpload::on_pushButton_5_clicked()
{
    this->hide();
}
