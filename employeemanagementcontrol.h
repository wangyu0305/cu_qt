#ifndef EM_H
#define EM_H

#include <QDialog>
#include <QSqlQuery>
#include <QStandardItemModel>
#include "employeecontrol.h"
#include "accesscontrol.h"
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
namespace Ui {
class EmployeeManagementControl;
}

class EmployeeManagementControl : public QDialog
{
    Q_OBJECT

public:
    explicit EmployeeManagementControl(QWidget *parent = 0);
    void selectAll();
    void setTabelHeader();
    void viewAll();
    ~EmployeeManagementControl();

private slots:
    void on_pushButton_3_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_12_clicked();

    void on_pushButton_11_clicked();

private:
    Ui::EmployeeManagementControl *ui;
    QStandardItemModel *model;
    employeeControl emc;
    accessControl AC;
    QSqlQuery myqsql;
};

#endif // EM_H
