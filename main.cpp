#include "mainwindow.h"
#include <QApplication>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QSqlQuery>
#include "defs.h"
#include <QDebug>
#include <QSqlError>
#include "employee.h"
#include "roles.h"
#include "payment.h"
#include "faculty.h"
#include "stuff.h"
#include "ta.h"
#include "ra.h"
#include "term.h"
#include "continuing.h"

/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/

static bool createConnect();
bool createEmployees();
QString employeeSql(int eid, QString name, int age);
QString roleSql(int rid, int eid, QString rolename, QString period, QString type, int salary);
QString paySql(int pid, int eid, int rid, float salary, float deduction, float raise, float netincome, QString stime, QString etime);

void transConnection();
void createTransction();

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    if(!createConnect())
        return -1;
    MainWindow w;
    w.show();

    return a.exec();
}

static bool createConnect(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("cakeisland");
    if(!db.open()){
        QMessageBox::critical(0, qApp->tr("Cannot open database"),
                    qApp->tr("Unable to establish a database connection.\n"
                             "This example needs SQLite support. Please read "
                             "the Qt SQL driver documentation for information how "
                             "to build it.\n\n"
                             "Click Cancel to exit."), QMessageBox::Cancel);
                return false;
    }
    // ignore existing table
    if(db.tables().contains("employees")){
        return true;
    }

    QSqlQuery query;

    // drop existing table
    query.exec("drop table employees;");
    query.exec("drop table payments;");
    query.exec("drop table roles;");

       query.exec("create table employees (id int primary key, "
                  "name varchar(20), age int)");

       query.exec("create table payments (id int primary key, eid int, rid int, grossincome float, deduction float, raise float, amount float,"
                  "startdate DATE, enddate DATE, foreign key(eid) references employees (id) on delete cascade, foreign key(rid) references roles (id) on delete cascade)");

       query.exec("create table roles (id int primary key, eid int, type varchar(20), jobtitle varchar(20),"
                  "jobduration varchar(20), jobsalary float, raise float default 0, foreign key(eid) references employees (id) on delete cascade)");
      if(!createEmployees())
          qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();

      transConnection();
      //createTransction();
      return true;
}


bool createEmployees(){
    QSqlQuery query;
    int age = 18;
    QString name = "Jony";
    int eid = IDS::getEID();
    int rid = IDS::getRID();
    int pid = IDS::getPID();
    float salary = 5000;
    float salaryPerMonth = salary /4;
    float deduction = 0;
    float raise = 0;
    float netincome = salaryPerMonth + raise - deduction;

    FullTime fulltime;
    PartTime parttime;
    JobPeriod jobparttime;
    JobPeriod jobfulltime;

    JobType term;
    JobType continuing;

    TAs ta(rid, eid, term, parttime);
    RAs ra(rid, eid, term, parttime);
    Faculty fa(rid, eid, continuing);
    Stuff stuff(rid, eid, term, jobparttime);
    Stuff stuff2(rid, eid, continuing, jobparttime);
    Stuff stuff3(rid, eid, continuing, jobfulltime);


    QString r = roleSql(rid, eid, "faculty", "fulltime", "term", salary);
    QString p = paySql(pid, eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
    //qDebug() << p;
    if(!query.exec(p))qDebug() << "SqLite error insert payments:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();

    p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
    query.exec(p);
    p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
    query.exec(p);
    p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
    query.exec(p);

    QString q = employeeSql(eid, name, age++);
    query.exec(q);
    query.exec(r);

    eid = IDS::getEID();
    rid = IDS::getRID();
    name = "timmy";
     p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
    query.exec(p);
    p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
    query.exec(p);
    p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
    query.exec(p);
    p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");

     q = employeeSql(eid, name, age++);
     r = roleSql(rid, eid, "faculty", "fulltime", "continuing", salary);

     if(!query.exec(q)) qDebug() << "SqLite error insert:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
     if(!query.exec(r))qDebug() << "SqLite error insert role:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
     if(!query.exec(p))qDebug() << "SqLite error insert payments:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();


    eid = IDS::getEID();
    rid = IDS::getRID();
    name = "beer";
    q = employeeSql(eid, name, age++);
    r = roleSql(rid, eid, "stuff", "fulltime", "continuing", salary);
    p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
    query.exec(p);
    p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
    query.exec(p);
    p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
    query.exec(p);
    p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");

     query.exec(p);
    query.exec(q);
    query.exec(r);

    rid = IDS::getRID();
     r = roleSql(rid, eid, "ta", "parttime", "term", salary);

     query.exec(r);
     p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
     query.exec(p);
     p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
     query.exec(p);
     p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
     query.exec(p);
     p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
     query.exec(p);

     eid = IDS::getEID();
     rid = IDS::getRID();
     name = "tiger";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "stuff", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
     query.exec(q);
     query.exec(r);

     rid = IDS::getRID();
      r = roleSql(rid, eid, "ra", "parttime", "term", salary);

      query.exec(r);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "ramsey";
       q = employeeSql(eid, name, age++);
       r = roleSql(rid, eid, "stuff", "parttime", "term", salary);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
       query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "bob";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "stuff", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      rid = IDS::getRID();
       r = roleSql(rid, eid, "ta", "parttime", "term", salary);

       query.exec(r);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
       query.exec(p);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "shelley";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ra", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);
      rid = IDS::getRID();
       r = roleSql(rid, eid, "ta", "parttime", "term", salary);

       query.exec(r);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
       query.exec(p);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "randy";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "stuff", "parttime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);
      rid = IDS::getRID();
       r = roleSql(rid, eid, "ta", "parttime", "term", salary);

       query.exec(r);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
       query.exec(p);
       p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
       query.exec(p);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "tiny";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "stuff", "fulltime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");

      if(!query.exec(q)) return false;
      if(!query.exec(r))return false;
      if(!query.exec(p)) return false;

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "sober";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "stuff", "parttime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "emma";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ta", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "rola";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ra", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "zoro";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "faculty", "fulltime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "robin";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "faculty", "fulltime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "sanji";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "faculty", "fulltime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "nami";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "faculty", "fulltime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "godusp";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "faculty", "fulltime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "franky";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "faculty", "fulltime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "brook";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "faculty", "fulltime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "monkey";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "faculty", "fulltime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "law";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "stuff", "parttime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "ivanka";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "stuff", "parttime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "mr.3";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "stuff", "parttime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "mr.2";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "stuff", "parttime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "vivi";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "stuff", "parttime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "sunny";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "stuff", "parttime", "continuing", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "jinbei";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ta", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "bigmom";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ra", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "redhair";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ra", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "whitebeard";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ra", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "blackbeard";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ra", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "kaido";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ra", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "dragon";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ra", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salary, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salary, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salary, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salary, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "gap";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ra", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "kobi";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ta", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "shark";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ta", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "ace";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ta", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "sabo";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ta", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "hawki";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ta", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "momo";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ta", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "naruto";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ta", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "kakasi";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ta", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

      eid = IDS::getEID();
      rid = IDS::getRID();
      name = "sasiki";
      q = employeeSql(eid, name, age++);
      r = roleSql(rid, eid, "ta", "parttime", "term", salary);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-5-1", "2016-6-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-6-1", "2016-7-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-7-1", "2016-8-1");
      query.exec(p);
      p = paySql(IDS::getPID(), eid, rid, salaryPerMonth, deduction, raise, netincome, "2016-8-1", "2016-9-1");
      query.exec(p);
      query.exec(q);
      query.exec(r);

    return true;
}


QString roleSql(int rid, int eid, QString rolename, QString period, QString type, int salary){
    return "insert into roles values(" + QString::number(rid) + ", " + QString::number(eid) + ", '" + rolename + "'" + ", '" + period + "'" + ", '" + type + "'" + ", " + QString::number(salary) + ", 0)";
}

QString paySql(int pid, int eid, int rid, float salary, float deduction, float raise, float netincome, QString stime, QString etime){
    return "insert into payments values(" + QString::number(pid) + ", " + QString::number(eid) + ", " + QString::number(rid) + ", " + QString::number(salary) + ", " + QString::number(deduction) + ", " + QString::number(raise) + ", " + QString::number(netincome) +
            ", '" + stime + "', '" + etime +"')";
}

QString employeeSql(int eid, QString name, int age){
    return "insert into employees values(" + QString::number(eid) + ", '" + name + "', " + QString::number(age) + ")";
}

void transConnection(){
    QSqlQuery query;

    // drop existing table
    query.exec("drop table employeesTrans;");
    query.exec("drop table paymentsTrans;");
    query.exec("drop table rolesTrans;");
    query.exec("drop table transactions;");

    query.exec("create table transactions (id int primary key, eid int, rid int, "
               "type varchar(20), date varchar(20) )");

       query.exec("create table employeesTrans (id int primary key, "
                  "name varchar(20), age int)");

       query.exec("create table paymentsTrans (id int primary key, eid int, rid int, grossincome float, deduction float, raise float, amount float,"
                  "startdate DATE, enddate DATE, foreign key(eid) references employees (id) on delete cascade, foreign key(rid) references roles (id) on delete cascade)");

       query.exec("create table rolesTrans (id int primary key, eid int, type varchar(20), jobtitle varchar(20),"
                  "jobduration varchar(20), jobsalary float, raise float default 0, foreign key(eid) references employees (id) on delete cascade)");

}

QString createTrans(int id, int eid, int rid, QString type){
    return "insert into transactions values(" + QString::number(id) + ", " + QString::number(eid) + " , " + QString::number(rid) +  ", '" + type + "', '2017-3-12')";
}

void createTransction(){
    QSqlQuery query;
    int age = 18;
    QString name = "blackbeard";
    int eid = 1030;
    int rid = 10001;
    int pid = 100001;
    int tid = 1;
    float salary = 5000;
    float salaryPerMonth = salary / 4;
    float deduction = 0;
    float raise = 0;
    //float netincome = salary + raise - deduction;

    //FullTime fulltime;
    //PartTime parttime;
    //JobPeriod jobparttime;
    //JobPeriod jobfulltime;

    //JobType term;
    //JobType continuing;

    Employee em(eid, name, age++);
    TAs ta(rid, eid, salary);
    Payment pay1(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    Payment pay2(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    Payment pay3(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    Payment pay4(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(ta.TAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update-addrole"));

    name = "kaido";
    em.setEmployee(++eid, name, age++);
    RAs ra(++rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(ra.RAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update-addrole"));

    name = "dragon";
    em.setEmployee(++eid, name, age++);
    RAs ra1(++rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(ra1.RAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update-addrole"));

    name = "gap";
    em.setEmployee(++eid, name, age++);
    RAs ra2(++rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(ra2.RAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update"));

    name = "kobi";
    em.setEmployee(++eid, name, age++);
    Faculty fac(++rid, eid, "term", salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(fac.FacultyString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update"));

    name = "shark";
    em.setEmployee(++eid, name, age++);
    RAs ras(++rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(ras.RAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update"));

    name = "ace";
    em.setEmployee(++eid, name, age++);
    RAs ra1s(++rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(ra1s.RAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update"));

    name = "sabo";
    em.setEmployee(++eid, name, age++);
    RAs ra2s(++rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(ra2s.RAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update"));

    name = "hawki";
    em.setEmployee(++eid, name, age++);
    Faculty facs(++rid, eid, "term", salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(facs.FacultyString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update-addpay"));

    name = "momo";
    em.setEmployee(++eid, name, age++);
    Faculty fac1(++rid, eid, "term", salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(fac1.FacultyString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update-addpay"));

    name = "labom";
    em.setEmployee(++eid, name, age++);
    Stuff stu(++rid, eid, "fulltime", "term", salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(stu.stuffString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update-addpay"));


    name = "naruto";
    em.setEmployee(++eid, name, age++);
    Stuff stu1(++rid, eid, "parttime", "term", salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(stu1.stuffString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update-adjustpay"));

    name = "kakasi";
    em.setEmployee(++eid, name, age++);
    Stuff stu2(++rid, eid, "fulltime", "continuing", salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(stu2.stuffString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update-adjustpay"));

    name = "sasiki";
    em.setEmployee(++eid, name, age++);
    Stuff stu3(++rid, eid, "parttime", "term", salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(stu3.stuffString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "update-adjustpay"));

    eid = 1100;

    name = "lasviga";
    em.setEmployee(++eid, name, age++);
    TAs taa(rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(taa.TAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "add"));

    name = "tomad";
    em.setEmployee(++eid, name, age++);
    RAs raa(++rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(raa.RAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "add"));

    name = "evadan";
    em.setEmployee(++eid, name, age++);
    Faculty faca(++rid, eid, "term", salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(faca.FacultyString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "add"));

    name = "lasboom";
    em.setEmployee(++eid, name, age++);
    Stuff stua(++rid, eid, "fulltime", "term", salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(stua.stuffString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "add"));

    name = "lasvigasda";
    em.setEmployee(++eid, name, age++);
    TAs taaa(rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(taaa.TAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "add"));

    name = "tomadanda";
    em.setEmployee(++eid, name, age++);
    RAs raaa(++rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(raaa.RAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "add"));

    name = "evadasan";
    em.setEmployee(++eid, name, age++);
    Faculty facaa(++rid, eid, "term", salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(facaa.FacultyString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "add"));

    name = "lasanboom";
    em.setEmployee(++eid, name, age++);
    Stuff stuaa(++rid, eid, "fulltime", "term", salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(stuaa.stuffString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "add"));

    name = "newgate";
    em.setEmployee(++eid, name, age++);
    TAs taag(rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(taag.TAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "add"));

    name = "edwar";
    em.setEmployee(++eid, name, age++);
    RAs raaw(++rid, eid, salary);
    pay1.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-5-1", "2016-6-1");
    pay2.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-6-1", "2016-7-1");
    pay3.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-7-1", "2016-8-1");
    pay4.setPayment(pid++, eid, rid, salaryPerMonth, deduction, raise, "2016-8-1", "2016-9-1");
    query.exec(em.employeeString());
    query.exec(raaw.RAString());
    query.exec(pay1.paymentString());
    query.exec(pay2.paymentString());
    query.exec(pay3.paymentString());
    query.exec(pay4.paymentString());
    query.exec(createTrans(tid++, eid, rid, "add"));

    eid = 1010;

    query.exec(createTrans(tid++, eid++, 0, "remove"));

    query.exec(createTrans(tid++, eid++, 0, "remove"));

    query.exec(createTrans(tid++, eid++, 0, "remove"));

    query.exec(createTrans(tid++, eid++, 0, "remove"));

    query.exec(createTrans(tid++, eid++, 0, "remove"));

    query.exec(createTrans(tid++, eid++, 0, "remove"));

    query.exec(createTrans(tid++, eid++, 0, "remove"));
}
