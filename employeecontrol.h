#ifndef EMPLOYEECONTROL_H
#define EMPLOYEECONTROL_H

#include "employee.h"
#include "roles.h"
#include <QString>
#include <QStringList>


class employeeControl
{
public:
    employeeControl();
    ~employeeControl();
    void createEmp(int, QString, int, int, QString, QString, QString);
    bool find(int);
    bool existRole(int, int);
    void removeEmp(int);
    void addRole(int, int, QString, QString, QString);
    void modifyRole(int, int, QString, QString, QString);
    void removeRole(int, int);
    void makePay(int pid, int eid, int rid, float s, float d, float r, QString sdate, QString edate);
    void deduction(int pid, int eid, int rid, float d);
    void deduction(int pid, int eid, int rid, int d);
    void raise(int pid, int eid, int rid, float r);
    void raise(int pid, int eid, int rid, int r);
    void merge(QStringList);

private:
    Employee** ems;
    int counter;
};

#endif // EMPLOYEECONTROL_H
