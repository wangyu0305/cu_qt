#ifndef ADDROLE_H
#define ADDROLE_H
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include <QDialog>

namespace Ui {
class addRole;
}

class addRole : public QDialog
{
    Q_OBJECT

public:
    explicit addRole(QWidget *parent = 0);
    void setId(int);
    ~addRole();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::addRole *ui;
    int eid;
};

#endif // ADDROLE_H
