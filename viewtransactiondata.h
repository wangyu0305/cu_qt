#ifndef VIEWTRANSACTIONDATA_H
#define VIEWTRANSACTIONDATA_H

#include <QDialog>

/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/

namespace Ui {
class viewTransactionData;
}

class viewTransactionData : public QDialog
{
    Q_OBJECT

public:
    explicit viewTransactionData(QWidget *parent = 0);
    ~viewTransactionData();
    void viewData();

private slots:
    void on_pushButton_clicked();

private:
    Ui::viewTransactionData *ui;
};

#endif // VIEWTRANSACTIONDATA_H
