#ifndef NEWEMP_H
#define NEWEMP_H
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include <QDialog>

namespace Ui {
class newEmp;
}

class newEmp : public QDialog
{
    Q_OBJECT

public:
    explicit newEmp(QWidget *parent = 0);
    ~newEmp();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::newEmp *ui;
};

#endif // NEWEMP_H
