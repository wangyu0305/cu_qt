#ifndef CONTINUING
#define CONTINUING
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include "jobtype.h"
#include "defs.h"

class Continuing: public JobType{
private:
    string stime;
    JobTitleType type;

public:
    Continuing(string s){ stime = s;}
    JobTitleType getType(){return type;}
    void setType(JobTitleType t){ type = t;}
    string getTypeString(){return "continuing";}
    string getSTime(){return stime;}
    void setTime(string stime){
        this->stime = stime;
    }

    void setSalary(float amount){salary = amount;}
    float getSalary(){return salary;}
    void raisePer(int p){this->salary += this->salary * p/100;}
    void raiseAmount(float a){this->salary += a;}

};

#endif // CONTINUING

