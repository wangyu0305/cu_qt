#ifndef TERM
#define TERM
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include "jobtype.h"
#include "defs.h"

class Term: public JobType{

private:
    JobDurationType type;
    string stime;
    string etime;

public:
    Term(){}
    Term(string s, string e){
        stime = s;
        etime = e;
    }
    JobDurationType getType(){return type;}
    void setType(JobDurationType t){ type = t;}
    string getTypeString(){return "term";}
    void setTime(string stime, string etime){
        this->stime = stime;
        this->etime = etime;
    }
    void setSalary(float amount){ salary = amount;}
    float getSalary(){return salary;}
    string getSTime(){return stime;}
    string getETime(){ return etime;}
    float payByEvenMonth(int m){
        int amount;
        amount = (salary / 12)*m;
        return amount;
    }
};

#endif // TERM

