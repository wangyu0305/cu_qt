#ifndef PGT_H
#define PGT_H
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include <QDialog>

namespace Ui {
class viewPayrollGenerateForm;
}

class viewPayrollGenerateForm : public QDialog
{
    Q_OBJECT

public:
    explicit viewPayrollGenerateForm(QWidget *parent = 0);
    ~viewPayrollGenerateForm();

private slots:
    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::viewPayrollGenerateForm *ui;
};

#endif // PGT_H
