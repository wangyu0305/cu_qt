#author: ryan wang
/*
run on linux 32
require Qt 5.3 or newer
require qmake, make
require sqlite
*/

To launching the CuNics program:
    ./CuNics

To complie the program:
    make

purpose of program:
	for university employees management, payment generate, view paystub, data transfer.


Use of the program:
	1, employee management: launch the program, enter as ps users, clich "employee management", 
		1.1 view all employee only (click "view all employee")
		1.2 view all roles in entire system (clich "view all roles")
		1.3 filter view, (emp: filter -> faculty, click -> view )
		1.4 add new employee (click add new employee, fill employee information, click ok, back, click -> save (can view result))
		1.5 add new role to exist employee (input employee ID, fill role information, click ok, back, click -> save (can view result), (when you check remember your input employee id))
		1.6 remove a role from exist employee (input role ID, click apply, click save (can view result), better not remove an employee with only one role)
		1.7 remove an employee (input employee ID, click apply, click save (can view result))
		1.8 raise to continuing (input raise amount or percentage, select which combination you want to give raise, click apply, click save (can view result))

	2, payroll generate: launch the program, enter as ps user, click "payroll generate tool", 
		2.1 fill payment information, (by default no deduction, no correct, and with an default pay period 4 month), select which type to pay or directly pay to an individual by input employee ID and role ID, click apply, view result, click ok. (Note after payroll generate, can login as employee to view paystub).

	3, view pay stub: launch the program, enter as employee,
		3.1 input an employee ID to view paystub information related to that employee, (by default, every employee have 4 month pay stub)
		3.2 select special date to view paystub (an employee may not have been paid during that time, so it could be no relust).

	4, data transfer (update information from outside of the system): launch the program, enter as ps user, click "data integration tool",
		4.1 click "upload data", to view all transfer data, (you can edit data at view table or transactions.txt source file, but make sure not to violate transaction data rules, as list below), click "merge" to merge data with existing information, after merge, click view result to view result after merge, (before merge, you can first click view result to view existing information before merge, and then click merge, click view result to view the difference)

	
	(*if you want make change make sure you follow this rule)
	transaction data rules: our transaction data seperated by comma ",", have 17 area in total, if there's no data the area can be 0 or blank, anyway make sure there're 16 ",".
	area 1: transaction ID, indicate this transaction record number.
	area 2: transaction type, indicate what type is this transaction including update-addrole (add role to existing employee with or without pay information), update (update employee information, can change employee name age "but not ID", employee role with or without changed pay information), update-addpay (add pay information to existing employee and existing role related to that employee), update-adjust (adjust sepcific employee role salary, employee have to be exist and have that role), update-raise (raise salary to sepcfic employee with sepcific role, can be amount or percentage, the difference is percentage alway less than 100), add (add new employee, could be different roles combination, with or without pay information), remove-role (remove a role from existing employee, can leave other area blank or 0 if it's not needed), remove (remove an existing employee).
	area 3: transaction date, indicate what day made this transaction file.
	area 4: employee ID (unique).
	area 5: employee name.
	area 6: employee age.
	area 7: role salary, indicate basic salary of this role of the employee.
	area 8: role: indicate role name (ta, ra, faculty or stuff)
	area 9: job type: indicate job type fulltime or parttime.
	area 10: job title: indicate job period term or continuing.
	area 11: role ID (unique):
	area 12: payment ID (unique):
	area 13: income: indicate how much net income made within this pay period.
	area 14: raise: indicate how much raise to this role of the employee.
	area 15: deduction: indicate how much lost within this pay period (like paid tax, subtract other pay...)
	area 16: start date: indidate start date of this pay period.
	area 17: end date: indicate end date of this pay period.


For Visual Studio users, qmake can also generate '.dsp' files, for example:

    qmake -t vcapp -o CuNics.dsp CuNics.pro
