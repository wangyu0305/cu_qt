/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include "editemployee.h"
#include "ui_editemployee.h"
#include <QSqlQuery>
#include <QMessageBox>
#include <QSqlError>
#include <QDate>
#include <QDebug>
#include "employee.h"
#include "roles.h"

editEmployee::editEmployee(QWidget *parent, int id) :
    QDialog(parent), eid(id),
    ui(new Ui::editEmployee)
{
    ui->setupUi(this);
    initDisplay();
}

editEmployee::~editEmployee()
{
    delete ui;
}

void editEmployee::setId(int eid){
    this->eid = eid;
}

void editEmployee::initDisplay(){
    QSqlQuery query;
    QString q = "select employees.name, employees.age, roles.jobsalary, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id=roles.eid and employees.id="+QString::number(eid)+" group by roles.id";
    query.exec(q);

    int numberOfRows = 0;
    if(query.last())
    {
        numberOfRows =  query.at() + 1;
        query.first();
        query.previous();
    }
    //qDebug() << numberOfRows;
    for(int i = 0; i < numberOfRows; i++){
        ui->comboBox_5->addItem(QString::number(i));
    }
    query.next();
    display(query);
}

void editEmployee::display(QSqlQuery query){

    ui->lineEdit_2->setText(query.value(0).toString());
    ui->lineEdit_3->setText(query.value(1).toString());
    ui->lineEdit->setText(query.value(2).toString());
    QString val = query.value(3).toString();
    if(val == "faculty")
        ui->comboBox->setCurrentIndex(0);
    else if(val == "staff")
        ui->comboBox->setCurrentIndex(1);
    else if(val == "ta")
        ui->comboBox->setCurrentIndex(2);
    else if(val == "ra")
        ui->comboBox->setCurrentIndex(3);
    else
        ui->comboBox->setCurrentIndex(0);
    val = query.value(4).toString();
    if(val == "fulltime")
        ui->comboBox_2->setCurrentIndex(0);
    else if(val == "parttime")
        ui->comboBox_2->setCurrentIndex(1);
    else
        ui->comboBox_2->setCurrentIndex(0);
    val = query.value(5).toString();
    if(val == "term")
        ui->comboBox_3->setCurrentIndex(0);
    else if(val == "continuing")
        ui->comboBox_3->setCurrentIndex(1);
    else
        ui->comboBox_3->setCurrentIndex(0);
//    ui->dateEdit->setDate(QDate::fromString(query.value(6).toString(), "yyyy-MM-dd"));
//    ui->dateEdit_2->setDate(QDate::fromString(query.value(7).toString(), "yyyy-MM-dd"));
//    val = query.value(8).toString();
//    if(val == "winter")
//        ui->comboBox_4->setCurrentIndex(0);
//    else if(val == "spring")
//        ui->comboBox_4->setCurrentIndex(1);
//    else if(val == "summer")
//        ui->comboBox_4->setCurrentIndex(2);
//    else if(val == "fall")
//        ui->comboBox_4->setCurrentIndex(3);
//    else
//        ui->comboBox_4->setCurrentIndex(0);
    qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
}

void editEmployee::on_comboBox_5_currentIndexChanged(const QString &arg1)
{
    QSqlQuery query;
    QString q = "select employees.name, employees.age, roles.jobsalary, roles.type, roles.jobtitle, roles.jobduration, roles.id from employees, roles where employees.id=roles.eid and employees.id="+QString::number(eid)+" group by roles.id";
    //query.exec(q);
    if(!query.exec(q)){
        qDebug() << "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number();
    }
    query.seek(arg1.toInt());
    rid = query.value(6).toInt();
    //qDebug() << rid;
    display(query);
}

void editEmployee::on_pushButton_2_clicked()
{
    QSqlQuery query;
    QString name = ui->lineEdit_2->text();
    QString age = ui->lineEdit_3->text();
    QString role = ui->comboBox->currentText();
    QString title = ui->comboBox_2->currentText();
    QString duration = ui->comboBox_3->currentText();
//    QString term = ui->comboBox_4->currentText();
    QString salary = ui->lineEdit->text();
//    int year = ui->dateEdit->date().year();
//    int month = ui->dateEdit->date().month();
//    int day = ui->dateEdit->date().day();
//    int endyear = ui->dateEdit_2->date().year();
//    int endmonth = ui->dateEdit_2->date().month();
//    int eday = ui->dateEdit_2->date().day();
    //Employee em;
    if(role == "Faculty")
        role = "faculty";
    else if(role == "Staff")
        role = "stuff";
    else if(role == "TA")
        role = "ta";
    else
        role = "ra";
    if(title == "FullTime")
        title = "fulltime";
    else
        title = "parttime";
    if(duration == "Term")
        duration = "term";
    else
        duration = "continuing";
//    if(term == "Winter")
//        term = "winter";
//    else if(term == "Spring")
//        term = "spring";
//    else if(term == "Summer")
//        term == "summer";
//    else if(term == "Fall")
//        term == "fall";
//    else
//        term = "";
    if(role == "faculty" && title != "fulltime"){
        QMessageBox::information(this, "error", "faculty can't be parttime, select again please!");
        return;
    }
    if((role == "ta" || role == "ra") && title != "parttime" && duration != "term"){
        QMessageBox::information(this, "error", "ta and ra must be parttime and term, select again please!");
        return;
    }
//    if(endyear < year || (endyear == year && endmonth < month)){
//        QMessageBox::information(this, "error", "date incorrect, select again please!");
//        return;
//    }
    if(salary.toFloat() <= 0){
        QMessageBox::information(this, "error", "salary incorrect, input int or float again please!");
        return;
    }

    //qDebug() << rid;
//    QString sdate = QString::number(year) + "-" + QString::number(month) + "-" + QString::number(day);
//    QString edate = QString::number(endyear) + "-" + QString::number(endmonth) + "-" + QString::number(eday);
    QString r = "update employees set name='" + name + "', age=" + age + " where id="+QString::number(eid);
    QString q = "update roles set type='"+ role +"', jobtitle='"+ title+"', jobduration='"+duration+"', jobsalary=" + salary + " where id="+QString::number(rid);
    //qDebug()<<q;
    //qDebug()<<r;
    //Roles ro = em.getRole(rid);
    //ro.setRoles(rid, eid);
    //em.addRole(ro);
    if(!query.exec(r)){
        QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
    }
    if(!query.exec(q)){
        QMessageBox::information(this, "error", "SqLite error:" + query.lastError().text() + ", SqLite error code:" + query.lastError().number());
    }
}

void editEmployee::on_pushButton_clicked()
{
    this->hide();
}
