#ifndef RA
#define RA
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include "term.h"
#include "parttime.h"
#include "roles.h"

class RAs: public Roles{
private:
    JobType type;
    PartTime pt;
    float salary;

public:
    RAs(int r, int e, JobType te, PartTime p): Roles(r, e), type(te), pt(p){}
    RAs(int r, int e, float s):Roles(r, e), salary(s){}
    void setRAs(int r, int e, float s){
        rid = r;
        eid = e;
        salary = s;
    }

    Payment findPay(int pid){
        for(int i = 0; i < counter; i++){
            if(pid == myPay[i].getPid()){
                return myPay[i];
            }
        }
    }

    Payment findPayByDate(QString date){
        for(int i = 0; i < counter; i++){
            if(date == myPay[i].getDate()){
                return myPay[i];
            }
        }
    }

    void addPay(Payment pay){ myPay[counter++] = pay;}
    int getId(){return rid;}
    int getEid(){return eid;}
    void correctincome(float amount){
        type.setSalary(amount);
    }
    void deduction(float amount){
        type.setSalary(type.getSalary() - amount);
    }
    void deductionPercent(int per){
        type.setSalary(type.getSalary() - (type.getSalary()/ (per)));
    }

    QString RAString(){
        return "insert into rolesTrans values(" + QString::number(rid) + ", " + QString::number(eid) + ", 'ra'" + ", 'parttime'" + ", 'term'" + ", " + QString::number(salary) + ", 0)";
    }
};

#endif // RA

