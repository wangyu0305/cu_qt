#ifndef EMPLOYEE
#define EMPLOYEE
#include <QString>
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include "roles.h"

class Employee{
private:
    int id;
    QString name;
    int age;
    float balance;
    Roles** rs;
    int counter;

public:
    Employee(int eid, QString name, int age){
        id = eid;
        this->name = name;
        this->age = age;
        counter = 0;
        rs = new Roles*[10];
    }

    ~Employee(){
        for(int i = 0; i < counter; i++){
            delete(rs[i]);
        }
        delete(rs);
    }

    int getEID(){
        return id;
    }

    QString getName(){
        return name;
    }

    void setName(QString name){
        this->name = name;
    }

    int getAge(){
        return age;
    }

    void setAge(int age){
        this->age = age;
    }

    void setEmployee(int eid, QString name, int age){
        id = eid;
        this->name = name;
        this->age = age;
    }

    bool addRole(Roles* r){
        rs[counter++] = r;
        return true;
    }

    void addRole(int rid, QString type, QString title, QString duration){
        rs[counter++] = new Roles(id, rid, type, title, duration);
    }

    void modifyRole(int rid, QString type, QString title, QString duration){
        for(int i = 0; i < counter; i++){
            if(rid == rs[i]->getId()){
                rs[i]->setRoles(type, title, duration);
            }
        }
    }

    Roles* getRole(int id){
        for(int i = 0; i < counter; i++){
            if(id == rs[i]->getId()){
                return rs[i];
            }
        }
    }

    bool existRole(int id){
        for(int i = 0; i < counter; i++){
            if(id == rs[i]->getId()){
                return true;
            }
        }
        return false;
    }

    void addPaytoRole(int pid, int eid, int rid, float s, float d, float r, QString sdate, QString edate){
        for(int i = 0; i < counter; i++){
            if(rs[i]->getId() == rid){
                rs[i]->addPay(pid, eid, rid, s, d, r, sdate, edate);
            }
        }
    }

    void raiseToRole(int pid, int rid, float r){
        for(int i = 0; i < counter; i++){
            if(rs[i]->getId() == rid){
                rs[i]->raise(pid, r);
            }
        }
    }

    void raiseToRole(int pid, int rid, int r){
        for(int i = 0; i < counter; i++){
            if(rs[i]->getId() == rid){
                rs[i]->raise(pid, r);
            }
        }
    }

    void deductionOnRole(int pid,int rid, float d){
        for(int i = 0; i < counter; i++){
            if(rs[i]->getId() == rid){
                rs[i]->deduction(pid, d);
            }
        }
    }

    void deductionOnRole(int pid,int rid, int d){
        for(int i = 0; i < counter; i++){
            if(rs[i]->getId() == rid){
                rs[i]->deductionPercent(pid, d);
            }
        }
    }

    void removeRole(int id){
        for(int i = 0; i < counter; i++){
            if(id == rs[i]->getId()){
                for(int j = i; j < counter; j++)
                    rs[j] = rs[j+1];
                counter--;
                break;
            }
        }
    }

    QString employeeString(){
        return "insert into employeesTrans values(" + QString::number(id) + ", '" + name + "', " + QString::number(age) + ")";
    }


};

#endif // EMPLOYEE

