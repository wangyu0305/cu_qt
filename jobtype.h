#ifndef JOBTITLE_H
#define JOBTITLE_H
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
class JobType{
protected:

    float salary;

public:
    void setSalary(float);
    float getSalary();
};

#endif // JOBTITLE_H

