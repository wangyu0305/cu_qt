#ifndef PSWINDOWS_H
#define PSWINDOWS_H

#include <QMainWindow>

namespace Ui {
    class PSWindows;
}

class PSWindows: public QMainWindow
{
    Q_OBJECT

public:
    explicit PSWindows(QWidget *parent = 0);
    ~PSWindows();

private:
    Ui::PSWindows *ui;
};

#endif // PSWINDOWS_H
