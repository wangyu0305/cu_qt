#ifndef STUFF
#define STUFF
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include "roles.h"
#include "jobtype.h"
#include "jobperiod.h"

class Stuff: public Roles{
private:
    JobType jobtype;
    JobPeriod jobp;
    float salary;
    QString period;
    QString title;

public:
    Stuff(int r, int e, JobType jtype, JobPeriod jp):Roles(r, e){
        jobtype = jtype;
        jobp = jp;
    }

    Stuff(int r, int e, QString p, QString t, float s):Roles(r, e), period(p), title(t), salary(s){}
    void setStuff(int r, int e, QString p, QString t, float s){
        rid = r;
        eid = e;
        salary = s;
        period = p;
        title = t;
    }

    JobType getType(){return jobtype;}
    JobPeriod getPeriod(){return jobp;}

    Payment findPay(int pid){
        for(int i = 0; i < counter; i++){
            if(pid == myPay[i].getPid()){
                return myPay[i];
            }
        }
    }

    Payment findPayByDate(QString date){
        for(int i = 0; i < counter; i++){
            if(date == myPay[i].getDate()){
                return myPay[i];
            }
        }
    }

    void addPay(Payment pay){ myPay[counter++] = pay;}
    int getId(){return rid;}
    int getEid(){return eid;}
    void correctincome(float amount){
        jobtype.setSalary(amount);
    }
    void deduction(float amount){
        jobtype.setSalary(jobtype.getSalary() - amount);
    }
    void deductionPercent(int per){
        jobtype.setSalary(jobtype.getSalary() - (jobtype.getSalary()/ (per)));
    }

    QString stuffString(){
        return "insert into rolesTrans values(" + QString::number(rid) + ", " + QString::number(eid) + ", 'stuff'" + ", '" + period +"'" + ", '"+ title +"'" + ", " + QString::number(salary) + ", 0)";
    }
};

#endif // STUFF

