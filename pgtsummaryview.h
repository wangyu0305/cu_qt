#ifndef PGTSUMMARY_H
#define PGTSUMMARY_H

#include <QDialog>

namespace Ui {
class PGTsummaryview;
}

class PGTsummaryview : public QDialog
{
    Q_OBJECT

public:
    explicit PGTsummaryview(QWidget *parent = 0);
    ~PGTsummaryview();

private:
    Ui::PGTsummaryview *ui;
};

#endif // PGTSUMMARY_H
