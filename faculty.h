#ifndef FACULTY
#define FACULTY
#include "roles.h"
#include "jobtype.h"
#include "fulltime.h"
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
class Faculty: public Roles{
private:
    JobType jobtype;
    FullTime fulltime;
    float salary;
    QString period;

public:
    Faculty(int r, int e, JobType jtype):Roles(r, e){
        jobtype = jtype;
    }
    Faculty(int r, int e, QString period, float salary):Roles(r, e), period(period), salary(salary){}
    void setFaculty(int r, int e, QString period, float salary){
        rid = r;
        eid = e;
        this->period = period;
        this->salary = salary;
    }

    JobType getType(){return jobtype;}
    FullTime getPeriod(){return fulltime;}

    Payment findPay(int pid){
        for(int i = 0; i < counter; i++){
            if(pid == myPay[i].getPid()){
                return myPay[i];
            }
        }
    }

    Payment findPayByDate(QString date){
        for(int i = 0; i < counter; i++){
            if(date == myPay[i].getDate()){
                return myPay[i];
            }
        }
    }

    void addPay(Payment pay){ myPay[counter++] = pay;}
    int getId(){return rid;}
    int getEid(){return eid;}
    void correctincome(float amount){
        jobtype.setSalary(amount);
    }
    void deduction(float amount){
        jobtype.setSalary(jobtype.getSalary() - amount);
    }
    void deductionPercent(int per){
        jobtype.setSalary(jobtype.getSalary() - (jobtype.getSalary()/ (per)));
    }

    QString FacultyString(){
        return "insert into rolesTrans values(" + QString::number(rid) + ", " + QString::number(eid) + ", 'faculty'" + ", 'fulltime'" + ", '" + period + "'" + ", " + QString::number(salary) + ", 0)";
    }
};

#endif // FACULTY

