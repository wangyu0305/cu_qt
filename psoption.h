#ifndef PS_H
#define PS_H
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include <QDialog>

namespace Ui {
class PSOption;
}

class PSOption : public QDialog
{
    Q_OBJECT

public:
    explicit PSOption(QWidget *parent = 0);
    ~PSOption();

private slots:
    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::PSOption *ui;
};

#endif // PS_H
