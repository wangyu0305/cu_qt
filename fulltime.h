#ifndef FULLTIME_H
#define FULLTIME_H
#include "jobperiod.h"
#include "defs.h"
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
class FullTime: JobPeriod{
private:
    JobTitleType type = FULLTIME;

public:
    JobTitleType getType(){return type;}
    string getTypeString(){return "fulltime";}
};

#endif // FULLTIME_H

