#ifndef ACCESSCONTROL_H
#define ACCESSCONTROL_H
#include <QString>
#include <QSqlQuery>

class accessControl
{
public:
    accessControl();
    QString selectAll();
    int countAll();
    void save(QSqlQuery);
    QString viewAndSaveEmp();
    QString viewAndSaveAll();
    QString viewAndSaveR();
    QString EmployeeSql(int eid, QString name, int age);
    QString RoleSql(int rid, int eid, QString rolename, QString period, QString type, int salary);
    QString PaymentSql(int pid, int eid, int rid, float salary, float deduction, float raise, float netincome, QString stime, QString etime);
    QString tEmployeeSql(int eid, QString name, int age);
    QString tRoleSql(int rid, int eid, QString rolename, QString period, QString type, int salary);
    QString tPaymentSql(int pid, int eid, int rid, float salary, float deduction, float raise, float netincome, QString stime, QString etime);
    QString createTrans(int id, int eid, int rid, QString type);
    QString remove(int);
    QString removeRole(int);
    QString removeAllRole(int);
    QString removePayment(int);
    QString removeAllPayment(int);
    QString raise(int, float);
    QString raisePer(int, int);
};

#endif // ACCESSCONTROL_H
