#include "viewpayrollsummarynoticy.h"
#include "ui_viewpayrollsummarynoticy.h"
#include "psoption.h"
#include <QSqlQuery>
#include <QDebug>
#include <QStandardItemModel>
#include <QSqlError>
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/

ViewPayrollSummaryNoticy::ViewPayrollSummaryNoticy(QWidget *parent, int max) :
    QDialog(parent),
    ui(new Ui::ViewPayrollSummaryNoticy)
{
    ui->setupUi(this);
    model = new QStandardItemModel(1, 11, this);
    setTableHeader();
    select(max);
}

ViewPayrollSummaryNoticy::~ViewPayrollSummaryNoticy()
{
    delete ui;
}

void ViewPayrollSummaryNoticy::setTableHeader(){
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Payment ID")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Employee ID")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Role")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Job Type")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("Job Title")));
    model->setHorizontalHeaderItem(5, new QStandardItem(QString("NetIncome")));
    model->setHorizontalHeaderItem(6, new QStandardItem(QString("Deduction")));
    model->setHorizontalHeaderItem(7, new QStandardItem(QString("Raise")));
    model->setHorizontalHeaderItem(8, new QStandardItem(QString("GrossIncome")));
    model->setHorizontalHeaderItem(9, new QStandardItem(QString("Start")));
    model->setHorizontalHeaderItem(10, new QStandardItem(QString("End")));
}

void ViewPayrollSummaryNoticy::select(int max){
    QSqlQuery query;

    QString q = "select payments.id, payments.eid, roles.type, roles.jobtitle, roles.jobduration, payments.grossincome, payments.deduction, payments.raise, payments.amount, payments.startdate, payments.enddate from payments, roles "
                " where roles.id = payments.rid "
                " and payments.id > " + QString::number(max) + " group by payments.id;";

    //qDebug() << q;
    if(!query.exec(q))
        qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
    QStandardItem *item;
    int i = 0, j = 0;
    while (query.next()) {
        j = 0;
        item = new QStandardItem(QString(query.value(0).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(1).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(2).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(3).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(4).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(5).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(6).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(7).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(8).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(9).toString()));
        model->setItem(i, j++, item);
        item = new QStandardItem(QString(query.value(10).toString()));
        model->setItem(i, j++, item);
        i++;
    }

    ui->tableView->setModel(model);
}

void ViewPayrollSummaryNoticy::on_pushButton_clicked()
{
    this->hide();
    //PSOption ps;
    //ps.setModal(true);
    //ps.exec();
}
