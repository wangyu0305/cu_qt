#ifndef ROLES
#define ROLES
#include <QString>
/*
 * author ryan wang
 *
 * for the purpose of QT and SE learning
*/
#include "payment.h"

class Roles{
protected:
    int eid;
    int rid;
    Payment myPay[50];
    QString type;
    QString title;
    QString duration;
    int salary;
    int counter;

public:
    Roles(){
        counter = 0;
    }
    Roles(int r, int e){
        rid = r;
        eid = e;
        counter = 0;
    }
    Roles(int r, int e, QString type, QString title, QString duration){
        rid = r;
        eid = e;
        this->type = type;
        this->title = title;
        this->duration = duration;
    }

    void setRoles(int r, int e){
        rid = r;
        eid = e;
    }

    void setRoles(QString type, QString title, QString duration){
        this->type = type;
        this->title = title;
        this->duration = duration;
    }

    Payment findPay(int id){
        for(int i = 0; i < counter; i++){
            if(id == myPay[i].getPid())
                return myPay[i];
        }
    }

    Payment findPayByDate(QString date){
        for(int i = 0; i < counter; i++){
            if(date == myPay[i].getDate())
                return myPay[i];
        }

    }

    void addPay(Payment pay){
        myPay[counter++] = pay;
    }

    void addPay(int pid, int eid, int rid, float s, float d, float r, QString sdate, QString edate){
        myPay[counter++] = Payment(pid, rid, eid, s, d, r, sdate, edate);
    }

    int getId(){
        return rid;
    }

    int getEid(){
        return eid;
    }

    void raise(int pid, float amount){
        for(int i = 0; i < counter; i++){
            if(myPay[i].getPid() == pid){
                myPay[i].setAmount(myPay[i].getAmount()+amount);
            }
        }
    }

    void raise(int pid, int per){
        for(int i = 0; i < counter; i++){
            if(myPay[i].getPid() == pid){
                float amount = myPay[i].getAmount()*per/100;
                myPay[i].setAmount(myPay[i].getAmount()+amount);
            }
        }
    }

    void correctincome(int pid, float amount){
        for(int i = 0; i < counter; i++){
            if(pid == myPay[i].getPid()){
                myPay[i].setAmount(amount);
            }
        }
    }

    void deduction(int pid, float amount){
        for(int i = 0; i < counter; i++){
            if(pid == myPay[i].getPid()){
                if(amount < myPay[i].getAmount()){
                    myPay[i].setAmount(0);
                }else{

                    myPay[i].setAmount(amount);
                }
            }
        }
    }

    void deductionPercent(int pid, int per){
        for(int i = 0; i < counter; i++){
            if(pid == myPay[i].getPid()){
                int amount =  myPay[i].getAmount() * per/100;
                if(amount < myPay[i].getAmount()){
                    myPay[i].setAmount(0);
                }else{

                    myPay[i].setAmount(amount);
                }
            }
        }
    }
};

#endif // ROLES

