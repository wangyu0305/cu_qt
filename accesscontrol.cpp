#include "accesscontrol.h"
#include <QSqlQuery>
#include <QVariant>
#include <QString>

accessControl::accessControl()
{

}

QString accessControl::selectAll(){
    return "select employees.id, employees.name, employees.age, sum(roles.jobsalary), count(roles.id), roles.type, roles.jobtitle, roles.jobduration, roles.id from employees, roles where employees.id = roles.eid group by employees.id;";
}

int accessControl::countAll(){
    QSqlQuery query;
    query.exec("select count(*) from employees;");
    query.next();
    int num = query.value(0).toInt();
    return num;
}

void accessControl::save(QSqlQuery query){
    query.prepare("submit;");
    query.executedQuery();
}

QString accessControl::viewAndSaveEmp(){
    return "select employees.id, employees.name, employees.age, roles.jobsalary, roles.id, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id = roles.eid group by employees.id;";
}

QString accessControl::viewAndSaveAll(){
    return "select employees.id, employees.name, employees.age, roles.jobsalary, roles.id, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id = roles.eid group by roles.id order by employees.id;";
}

QString accessControl::viewAndSaveR(){
    return "select employees.id, employees.name, employees.age, roles.jobsalary, roles.id, roles.type, roles.jobtitle, roles.jobduration from employees, roles where employees.id = roles.eid group by roles.id order by employees.id;";
}

QString accessControl::RoleSql(int rid, int eid, QString rolename, QString period, QString type, int salary){
    return "insert into roles values(" + QString::number(rid) + ", " + QString::number(eid) + ", '" + rolename + "'" + ", '" + period + "'" + ", '" + type + "'" + ", " + QString::number(salary) + ", 0)";
}

QString accessControl::PaymentSql(int pid, int eid, int rid, float salary, float deduction, float raise, float netincome, QString stime, QString etime){
    return "insert into payments values(" + QString::number(pid) + ", " + QString::number(eid) + ", " + QString::number(rid) + ", " + QString::number(salary) + ", " + QString::number(deduction) + ", " + QString::number(raise) + ", " + QString::number(netincome) +
            ", '" + stime + "', '" + etime +"')";
}

QString accessControl::EmployeeSql(int eid, QString name, int age){
    return "insert into employees values(" + QString::number(eid) + ", '" + name + "', " + QString::number(age) + ")";
}

QString accessControl::tRoleSql(int rid, int eid, QString rolename, QString period, QString type, int salary){
    return "insert into rolesTrans values(" + QString::number(rid) + ", " + QString::number(eid) + ", '" + rolename + "'" + ", '" + period + "'" + ", '" + type + "'" + ", " + QString::number(salary) + ", 0)";
}

QString accessControl::tPaymentSql(int pid, int eid, int rid, float salary, float deduction, float raise, float netincome, QString stime, QString etime){
    return "insert into paymentsTrans values(" + QString::number(pid) + ", " + QString::number(eid) + ", " + QString::number(rid) + ", " + QString::number(salary) + ", " + QString::number(deduction) + ", " + QString::number(raise) + ", " + QString::number(netincome) +
            ", '" + stime + "', '" + etime +"')";
}

QString accessControl::tEmployeeSql(int eid, QString name, int age){
    return "insert into employeesTrans values(" + QString::number(eid) + ", '" + name + "', " + QString::number(age) + ")";
}

QString accessControl::createTrans(int id, int eid, int rid, QString type){
    return "insert into transactions values(" + QString::number(id) + ", " + QString::number(eid) + " , " + QString::number(rid) +  ", '" + type + "', '2017-3-30')";
}

QString accessControl::remove(int eid){
    return "delete from employees where id=" + QString::number(eid);
}

QString accessControl::removeRole(int rid){
    return "delete from roles where id=" + QString::number(rid);
}

QString accessControl::removeAllRole(int eid){
    return "delete from roles where eid=" + QString::number(eid);
}

QString accessControl::removeAllPayment(int eid){
    return "delete from payments where eid=" + QString::number(eid);
}

QString accessControl::removePayment(int pid){
    return "delete from payments where id=" + QString::number(pid);
}

QString accessControl::raisePer(int rid, int p){

    float per = p/100.0;
    QString q = "update roles set raise=jobsalary*" + QString::number(per) + ", jobsalary=jobsalary + (jobsalary * "+QString::number(per)+") where id=" + QString::number(rid) + ";";
    return q;
}

QString accessControl::raise(int rid, float per){

    QString q = "update roles set raise=" + QString::number(per) + ", jobsalary=jobsalary + "+QString::number(per)+" where id=" + QString::number(rid) + ";";
    return q;
}

